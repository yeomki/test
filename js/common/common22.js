var console = window.console || {
    log : function() {
    },
    info : function() {
    },
    warn : function() {
    },
    error : function() {
    }
};

//바로구매 장바구니에서 프로모션 자세히보기레이어의 장바구니 버튼을 눌렀을때 응답 카트번호 저장
var cartNosForDirectCart = "";
var beautyLoginCnt = sessionStorage.getItem("lCnt");

$.namespace = function() {
    var a = arguments, o = null, i, j, d;
    for (i = 0; i < a.length; i = i + 1) {
        d = a[i].split(".");
        o = window;
        for (j = 0; j < d.length; j = j + 1) {
            o[d[j]] = o[d[j]] || {};
            o = o[d[j]];
        }
    }
    return o;
}

$.namespace("common");
common = {

    scrollPos : 0,
    
    cannotAccess : function() {
        alert('접근할 수 없습니다. 권한이 부족합니다.');
    },
    validateFieldNotEmpty : function(id, message) {
        var loginId = $(id).val();
        if ($.trim(loginId) == '') {
            alert(message);
            return false;
        }

        return true;
    },
    convertSystemToJtmpl : function(str){
        if(str){
            str = str.replace(/\n/gi, "<br/>");
            str = str.replace(/ /gi, "&nbsp;");
            return str;
        }
    },
    splitToEnterKey : function(str){
        if(str){
            var patt= /\n/g;

            if(patt.test(str)){
                str = str.split(/\r|\n/)[0];
            }
            return str;
        }
    },

    sessionClear:function(){
        var url = _baseUrl +"login/sessionClear.do";
        common.Ajax.getAjaxObj("POST", url, "");
        console.log("sessionClear=======");
    },

    loginChk : function(){
        var url= _baseUrl + "login/loginCheckJson.do";
        var loginResult = false;
        
        $.ajax({
            type: "POST",
            url: url,
            data: null,
            dataType : 'json',
            async: false,
            cache: false,
            success: function(data) {

                if(!data.result && data.url!=null){
                    window.location.href = _secureUrl + data.url + "?redirectUrl=" + encodeURIComponent(location.href);
                }

                loginResult = data.result;
            },
            error : function(a, b, c) {
                console.log(a);
                console.log(b);
                console.log(c);
            }
        });

        return loginResult;
    },


    isLogin : function(){
        var url= _baseUrl + "login/loginCheckJson.do";
        var loginResult = false;

        $.ajax({
            type: "POST",
            url: url,
            data: null,
            dataType : 'json',
            async: false,
            cache: false,
            success: function(data) {
                loginResult = data.result;
            },
            error : function() {
                loginResult = false;
            }
        });

        return loginResult;
    },
    
    isBeautyLoginCnt : function(){
        var url= _baseUrl + "login/loginCheckJson.do";
        
        $.ajax({
            type: "POST",
            url: url,
            data: null,
            dataType : 'json',
            async: false,
            cache: false,
            success: function(data) {
                beautyLoginCnt++;
            },
            error : function() {
                beautyLoginCnt = 0;
            }
        });

        return beautyLoginCnt;
    },

    fileChk : function(file){
        var maxSize = 5 * 1024 * 1024 ; // 5MB
        // var maxSize = 0 ;
        var fileFilter =/.(jpg|gif|png|jpeg)$/i;
        var fileType = "";
        var fileSize = 0;

        if(file != null && file != undefined){
            for(i = 0; i < file.size(); i++){
                if(file[i].value!=""){
                    fileType = file[i].files[0].name.slice(file[i].files[0].name.lastIndexOf("."));
                    fileSize = file[i].files[0].size;

                    /*
                     * console.log("파일["+i+"]>>"+file);
                     * console.log("파일Type["+i+"]>>"+fileType);
                     */

                    if(!fileType.match(fileFilter)){
                        alert("등록할 수 없는 파일 형식입니다.");
                        return false;
                    }

                    if(fileSize > maxSize){
                        alert("5MB미만의 이미지 파일만 첨부할 수 있습니다.");
                        return false;
                    }
                }else{
                    console.log("첨부된 파일이 없습니다.["+file[i].name+"]");
                    return false;
                }
            }
            return true;
        }
    },
    getNowScroll : function() {
        var de = document.documentElement;
        var b = document.body;
        var now = {};
        now.X = document.all ? (!de.scrollLeft ? b.scrollLeft : de.scrollLeft) : (window.pageXOffset ? window.pageXOffset : window.scrollX);
        now.Y = document.all ? (!de.scrollTop ? b.scrollTop : de.scrollTop) : (window.pageYOffset ? window.pageYOffset : window.scrollY);
        return now;
    },
    isEmpty : function(str) {
        if (str == undefined || str == null || str === "" || str == "undefined" || str == "null") {
            return true;
        } else {
            return false;
        }
    },
    /**
     * Validation 처리를 위한 Object
     */
    Validator : {
        /**
         * common.Validator.isNumber()
         *
         * 해당 필드 값이 숫자인지 여부를 검사 함.
         *
         * 사용 예)
         * common.Validator.isNumber("#elId", "값을 확인 하세요.");
         * 일때 다음의 동작 실행
         * if($("#elId").val() != 숫자) {
         *      alert("값을 확인 하세요.");
         *      $("#elId").focus();
         *    }
         *
         *
         *
         * @param jqPath : Element의 Jquery Path
         * @param message : 입력값 오류일때 표시할 Message 값
         *
         * @return true(정상) or false(오류)
         *
         *
         */
        isNumber : function(jqPath, message) {
            var obj = $(jqPath);
            var value = obj.val();
            var isNumber = value.isNumber();
            if(!(isNumber)) {
                alert(message);
                obj.focus();
            }
            return isNumber;
        }
    },
    /**
     * timeStamp 데이타를 날짜 포맷으로 변환
     */
    formatDate  : function (timeStamp, format){
        var newDate = new Date();
        newDate.setTime(timeStamp);
        var formatDate = newDate.format(format);

        return formatDate;
    },

    loadingLayerIntervalId : 0,

    showLoadingLayer : function(isOpacity) {
        
//        if (common.app.appInfo != undefined && common.app.appInfo.isapp) {
//            //앱인 경우 앱에서 띄우도록 함
//            common.app.callShowLoadingBar("Y");
//            return;
//        }
        
        if ($("#mLoading").length > 0) {
            return;
        }

        var style = "";
        if (isOpacity == false) {
            style = " style=\"opacity:0;\"";
        }

        var loadingHtml = "<div id=\"mLoading\"><span class=\"loading_ico\">로딩중</span></div>";//dim 관련 내용 삭제
        $("body").append(loadingHtml);

      //로딩이미지 배열
        var img_arr = [_imgUrl + 'comm/loading_1.png',
            _imgUrl + 'comm/loading_2.png',
            _imgUrl + 'comm/loading_3.png',
            _imgUrl + 'comm/loading_4.png',
            _imgUrl + 'comm/loading_5.png',
            _imgUrl + 'comm/loading_6.png'];

        var load_val = true;
        var arr_no = 1;
        var show = $('.loading_ico').show()

        $('.loading_ico').css({'background-image':'url('+ img_arr[0] +')'});
        common.loadingLayerIntervalId = setInterval(function(){
//            console.log("showLoadingLayer");
            if(load_val){
                $('.loading_ico').css({'transform':'rotateY(0deg)'});
                load_val = false;
            }else{
                if(arr_no >= 6) arr_no = 0;
                $('.loading_ico').css({'background-image':'url('+ img_arr[arr_no] +')', 'transform':'rotateY(0)'});
                arr_no ++;
                load_val = true;
            }
        }, 90);
        
        setTimeout(function() {
            common.hideLoadingLayer();
        }, 15000);
    },

    hideLoadingLayer : function() {
//        if (common.app.appInfo != undefined && common.app.appInfo.isapp) {
//            setTimeout(function() {
//                //앱인 경우 앱에서 띄우도록 함
//                common.app.callShowLoadingBar("N");
//            }, 500);
//            return;
//        } 
        
        if ($("#mLoading").length < 1) {
            return;
        }

        setTimeout(function() {
            clearInterval(common.loadingLayerIntervalId);
            $("#mLoading").remove();
        }, 500);
    },
//    showLoadingLayer : function(isOpacity) {
//        
////        if (common.app.appInfo != undefined && common.app.appInfo.isapp) {
////            //앱인 경우 앱에서 띄우도록 함
////            common.app.callShowLoadingBar("Y");
////            return;
////        }
//        
//        if ($("#mLoading").length > 0) {
//            return;
//        }
//
//        var style = "";
//        if (isOpacity == false) {
//            style = " style=\"opacity:0;\"";
//        }
//
//        var loadingHtml = "<div id=\"mLoading\"><span class=\"loading_ico\"><img src=\"" + _imgUrl + "comm/loading_64x64.gif\"/></span><div class=\"dim\"" + style + "></div></div>";
//        $("body").append(loadingHtml);
//
//
//    },
//
//    hideLoadingLayer : function() {
////        if (common.app.appInfo != undefined && common.app.appInfo.isapp) {
////            setTimeout(function() {
////                //앱인 경우 앱에서 띄우도록 함
////                common.app.callShowLoadingBar("N");
////            }, 500);
////            return;
////        }
//        
//        if ($("#mLoading").length < 1) {
//            return;
//        }
//
//        setTimeout(function() {
//            $("#mLoading").remove();
//        }, 500);
//    },

    popupCallback : '',

    popFullOpen : function (title, _popCallback) {
        $(window).scrollTop(0.0); //추가부분

        $('#pop-full-title').html(title);
        $('body').css({'background-color' : '#fff'}); /* 2016-12-12 퍼블리싱 수정 반영 */
        $('.popFullWrap').show();
        $('#mWrapper').hide();

        this.popupCallback = _popCallback;
    },

    popFullClose : function(){
        $('body').css({'background-color' : '#eee'}); /* 2016-12-12 퍼블리싱 수정 반영 */
        $('.popFullWrap').hide();
        $('.popFullWrap').removeClass('outline');//쿠폰 하이라이트 제거        
        $('#mWrapper').show();

        window.scroll(0, common.scrollPos);
        
        //웹 접근성 popfocus 삭제( 2017-05-11 )
        $("a[data-focus~=on]").focus(); // 표시해둔 곳으로 초점 이동
        $("button[data-focus~=on]").focus(); // 표시해둔 곳으로 초점 이동
        window.setTimeout(function(){
            $("a[data-focus~=on]").removeAttr("data-focus");
            $("button[data-focus~=on]").removeAttr("data-focus");
            var lyLocalScroll = localStorage.getItem('lyScrollSet');
            if(lyLocalScroll != null ) {
                $(document).scrollTop(lyLocalScroll);
                localStorage.removeItem('lyScrollSet');
            }
        },100); // 
        

        if(this.popupCallback == '' || typeof this.popupCallback != 'function') return;

        this.popupCallback();
    },

    popLayerOpen : function(IdName){
        
        sessionStorage.setItem("scrollY_popLayer", common.getNowScroll().Y);
        console.log(sessionStorage.getItem("scrollY_popLayer"));
        
        var winH = $(window).height()/2;
        var popLayer = ('#'+IdName);
        $(popLayer).find('.popCont').css({'max-height': winH});

        var popPos = $(popLayer).height()/2;
        var popWid = $(popLayer).width()/2;
        $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)}).show().parents('body').css({'overflow' : 'hidden'});
        $('.dim').show();
        
        $('.dim').bind('click', function(){
            common.popLayerClose(IdName);
        });
        //가세로 변경
        $(window).resize(function(){
            winH = $(window).height()/2;
           $(popLayer).find('.popCont').css({'max-height': winH});
            popPos =$(popLayer).height()/2;
            popWid = $(popLayer).width()/2;
            $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)})
        });
        //sns 별도처리
        if(IdName =='SNSLAYER'){
           $(popLayer).css({'left':'0' , 'margin-left': '0'})
        };
      
        $("body").css("overflow", "hidden");
        
        //앱 호출
        setTimeout(function() {
            common.app.callMenu("Y");
        }, 100);
    },
    
    /* 0001063: MO_상품 상세 페이지 > 레이어창 팝업시 화면 밀림 현상 */
    popLayerOpen2 : function(IdName){
        
        sessionStorage.setItem("scrollY_popLayer", common.getNowScroll().Y);
        console.log(sessionStorage.getItem("scrollY_popLayer"));
        
        var winH = $(window).height()/2;
        var popLayer = ('#'+IdName);
        var popInHd = $(popLayer).find('.popHeader').innerHeight();
        var winH_type = $(window).height() - (popInHd + 40);
        var popContCls = $(popLayer).find('.popCont');
        
        if(popContCls.hasClass('type_h') == true){
            $(popLayer).find('.popCont').css({'max-height': winH_type});
        } else {
            $(popLayer).find('.popCont').css({'max-height': winH});            
        }

        var popPos = $(popLayer).height()/2;
        var popWid = $(popLayer).width()/2;
        //$(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)}).show().parents('body').css({'overflow' : 'hidden'});
        $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)}).show();
        $('.dim').show();
        
        $('.dim').bind('click', function(){
            common.popLayerClose(IdName);
        });
        //가세로 변경
        $(window).resize(function(){
            winH = $(window).height()/2;
            var popInHd = $(popLayer).find('.popHeader').innerHeight();
            var winH_type = $(window).height() - (popInHd + 40);
            var popContCls = $(popLayer).find('.popCont');
            
            if(popContCls.hasClass('type_h') == true){
                $(popLayer).find('.popCont').css({'max-height': winH_type});
            } else {
                $(popLayer).find('.popCont').css({'max-height': winH});            
            }
            popPos =$(popLayer).height()/2;
            popWid = $(popLayer).width()/2;
            $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)})
        });
        //sns 별도처리
        if(IdName =='SNSLAYER'){
           $(popLayer).css({'left':'0' , 'margin-left': '0'})
        };
      
        //$("body").css("overflow", "hidden");
        
        //앱 호출
        setTimeout(function() {
            common.app.callMenu("Y");
        }, 100);
    },

    popLayerClose : function(IdName){
        var popLayer = ('#'+IdName);
//        $(popLayer).hide().parents('body').css({'overflow' : 'visible'});
        $(popLayer).hide();
        $("body").css("overflow", "visible");

        try{
            var varNowScrollY = parseInt(sessionStorage.getItem("scrollY_popLayer"));

            if((varNowScrollY > 0)){
                $(window).scrollTop(varNowScrollY);
                sessionStorage.removeItem("scrollY_popLayer");
            }
        }catch(e){}

        $('.dim').hide();
        
        //웹 접근성 popfocus 삭제 ( 2017-05-11 )
        $("a[data-focus~=on]").focus(); // 표시해둔 곳으로 초점 이동
        $("button[data-focus~=on]").focus(); // 표시해둔 곳으로 초점 이동
        window.setTimeout(function(){
            $("a[data-focus~=on]").removeAttr("data-focus");
            $("button[data-focus~=on]").removeAttr("data-focus");
        },100); // 

        //앱 호출
        setTimeout(function() {
            common.app.callMenu("N");
            
            // 바이오 로그인 팝업 닫은 경우 앱에 푸시 수신 동의 팝업 오픈 요청
            setTimeout(function() {
                if(IdName == 'touchLogin'){
                    common.app.callBioLoginAgrCallBack();
                }
            },400);
        }, 100);

    },
    
    popLayerOpenNoReSize : function(IdName){
        
        sessionStorage.setItem("scrollY_popLayer", common.getNowScroll().Y);
        console.log(sessionStorage.getItem("scrollY_popLayer"));
        
        var winH = $(window).height()/2;
        var popLayer = ('#'+IdName);

        var popPos = $(popLayer).height()/2;
        var popWid = $(popLayer).width()/2;
        $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)}).show().parents('body').css({'overflow' : 'hidden'});
        $('.dim').show();
        
        $('.dim').bind('click', function(){
            common.popLayerClose(IdName);
        });
        //가세로 변경
        $(window).resize(function(){
            winH = $(window).height()/2;
            popPos =$(popLayer).height()/2;
            popWid = $(popLayer).width()/2;
            $(popLayer).css({'left':'50%' , 'margin-left':-(popWid) , 'top':'50%' , 'margin-top':-(popPos)})
        });
        //sns 별도처리
        if(IdName =='SNSLAYER'){
           $(popLayer).css({'left':'0' , 'margin-left': '0'})
        };
      
        $("body").css("overflow", "hidden");
        
        //앱 호출
        setTimeout(function() {
            common.app.callMenu("Y");
        }, 100);
    },

    popFocus : function(){
        
        //초기 실행시 data-focus 삭제
        $("a[data-focus~=on]").removeAttr("data-focus");
        $("button[data-focus~=on]").removeAttr("data-focus");
        
        $("a, button").click(function(){
            var el = $(this);
            el.attr('data-focus','on'); // 레이어 팝업이 닫힐 때를 위한 표시 - 웹접근성
         
            /* modalPopup팝업 위치조정 */
            window.setTimeout(function(){
                
                if( $("#pop-full-wrap").css("display") == 'block' ){
                    var target = $("#pop-full-wrap");
                }else if( $("#LAYERPOP01").css("display") == 'block' ){
                    var target = $("#LAYERPOP01");
                }else if( $("#SNSLAYER").css("display") == 'block' ){
                    var target = $("#SNSLAYER");
                }else if ( $("#allMenu").hasClass("show") ){
                    var target = $("#allMenu");
                }else if ( $("#fulsizePop").css("display") == 'block'){
                    var target = $("#fulsizePop");
                }else{
                }
                
                if( target != undefined ){
                    
                    var win_height = $(window).height();
                    var pop_height = target.height();
                    var top_value = $(window).scrollTop() + (win_height - pop_height) /2;
                    
                    target.attr("tabindex","0");
                    target.focus();    
                }
            },500);// ajax 팝업을 고려한 딜레이 추가
            /* [end]: modalPopup팝업 위치조정 */
        });


    },

    /** 현재 스크롤 위치 저장  **/
    setScrollPos : function(scrollPos){
       common.scrollPos = $(document).scrollTop();
    },
    /** 레이어팝업 클릭 시, 스크롤 위치값 localstorage 저장  **/
    setScrollPos2 : function(){
        var scrollT = $(document).scrollTop();
        localStorage.setItem('lyScrollSet', scrollT);
    },
    /** 히스토리 백 버튼 Bind **/
    historyBackBind : function(){
        $("#backBtn").click(function(){
//            if(common.app.appInfo != undefined && common.app.appInfo.isapp){
//                common.app.historyBack();
//            }
            history.back();
        });
    },
    
    isLoginPageChk : function(url){
        if (history.state == null) {
            history.replaceState({status:"login"}, null, null);
            history.pushState({status:"loginCheck"}, null, null);
        }
        $(window).bind("popstate", function() {
            if (history.state != null && history.state.status == "login") {
                //if(!common.isEmpty(url)){
                //    console.log("url존재:"+url);
                //    location.href= url;
                //}else{
                //    console.log("url없음:"+url);
                    location.href = _baseUrl + "main/main.do";
                //}
            }
        }); 
    },

    setLazyLoad : function(type) {

        if (type == undefined  || type == "seq" ) {
            //lazyload - 이미지스크롤이벤트
//            $(document).find("img.seq-lazyload").lazyload({
//                //effect : "fadeIn",
//                event : "sequential"
//            });

            //로딩 된 이미지에 중복으로 lazyload를 바인드하지 않도록 하기 위해 클래스명 변경
//            $(document).find("img.seq-lazyload").removeClass("seq-lazyload").addClass("completed-seq-lazyload");

            lazyloadSeq.load("img.seq-lazyload", "seq-lazyload");
        }

        if (type == undefined  || type == "scr" ) {
            lazyloadSeq.load("img.scroll-lazyload", "scroll-lazyload");

////            lazyload - 이미지스크롤이벤트
//            $(document).find("img.scroll-lazyload").lazyload({
//                effect : "fadeIn",
//                threshold : 1000
//            });
//
//            //로딩 된 이미지에 중복으로 lazyload를 바인드하지 않도록 하기 위해 클래스명 변경
//            $(document).find("img.scroll-lazyload").removeClass("scroll-lazyload").addClass("completed-scroll-lazyload");
        }

        $(document).resize();
        
    },

    //  재입고 알림 팝업 오픈
    openStockAlimPop : function(goodsNo, itemNo){
        if ( common.loginChk() ){
            var url = _baseUrl + "goods/getAlertStockAjax.do";
            var data = {goodsNo : goodsNo, itemNo : itemNo};
            common.Ajax.sendRequest("POST",url,data,common._callBackAlearStockForm);
        }
    },

    //  재입고 알림 콜백
    _callBackAlearStockForm : function(res){
        $("#pop-full-wrap").html(res);
        common.popFullOpen("재입고알림 신청", "");
        
        //  재입고 팝업 닫기 버튼 클릭 시 이미지 기술서 pinchZoom Init 
        $(".btnClose").click(function(){
            $(".controlHolder").empty();
            $("div#timpHtml").find("div#TEMP_HTML").attr("style", "");
            $("div#test00").html($("div#timpHtml").html().replace("TEMP_HTML", "markerTest"));
            $("#markerTest").pinchzoomer();
        });
        
    },
    
    //  행사안내 팝업 오픈
    openEvtInfoPop : function( promNo, promKndCd, promCond, goodsNo, itemNo ){
        
        var quickYn = $(":input:radio[name=qDelive]:checked").val();
        if(typeof(quickYn) == "undefined"){
            quickYn = $("#quickYn").val();
        }
        
        var url = _baseUrl + "goods/getGoodsPromEvtInfoAjax.do";
        var data = {promNo : promNo, promKndCd : promKndCd, promCond : promCond, goodsNo : goodsNo, itemNo : itemNo, quickYn : quickYn};
        if ( promCond == '1+1' || promCond == 'A+B' || promCond == 'GIFT' ){
            common.Ajax.sendRequest("POST",url,data,common._callBackSimpleGoodsEvtInfo);    
        }else{
            common.Ajax.sendRequest("POST",url,data,common._callBackGoodsEvtInfo);
        }
    },
    
    //  행사안내 콜백 ( 1+1 이 아닐 경우 )
    _callBackGoodsEvtInfo : function(res){
        $("#pop-full-wrap").html(res);
        common.popFullOpen("행사안내", "");
    },
    
    // 행사안내 콜백 ( 1+1 일 경우 )
    _callBackSimpleGoodsEvtInfo : function(res){
        $("#layerPop").html(res);
        common.popLayerOpen("LAYERPOP01");
    },
    
    isMainHome : function() {
        try {
            if (location.href.endWith("/main/main.do#0")) {
                return true;
            } 
            
            return false;
        } catch(e) {
            return false;
        }
    },
    
    bindGoodsListLink : function(filterSelectorStr) {
        var classNm = filterSelectorStr == undefined ? "" : filterSelectorStr + " ";
        
        $(classNm + ".goodsList").unbind("click");
                
        $(classNm + ".goodsList").bind("click", function(e) {
            e.preventDefault();
            
            //판매종료 여부체크
            var allSoldOutChk = $(this).find(".img").children("span").hasClass("allsoldOut");
            
            if(allSoldOutChk == false){
                common.link.moveGoodsDetail($(this).attr("data-ref-goodsNo"), $(this).attr("data-ref-dispCatNo"));
            }
        });
        
        $(classNm + ".item").unbind("click");
        
        $(classNm + ".item").bind("click", function(e) {
            e.preventDefault();
            
            //판매종료 여부체크
            var allSoldOutChk = $(this).find(".img").children("span").hasClass("allsoldOut");
            
            if(allSoldOutChk == false){
                common.link.moveGoodsDetail($(this).attr("data-ref-goodsNo"), $(this).attr("data-ref-dispCatNo"));
            }
        });

        $(classNm + ".goodsListLogin").bind("click", function(e) {
            e.preventDefault();
            //로그인 성인체크 로그인성인
            common.link.moveGoodsDetail($(this).attr("data-ref-goodsNo"), $(this).attr("data-ref-dispCatNo"));
        });

        $(classNm + ".goodsListAuth").bind("click", function(e) {
            e.preventDefault();
            //로그인 성인체크
            common.link.moveGoodsDetail($(this).attr("data-ref-goodsNo"), $(this).attr("data-ref-dispCatNo"));
        });
    },
    
    /* 2018.12.18 바코드 스캔 추가 */
    bindGoodsListBarcodeLink : function(filterSelectorStr) { 
        var classNm = filterSelectorStr == undefined ? "" : filterSelectorStr + " ";
        
        $(classNm + ".goodsList").unbind("click");
                
        $(classNm + ".goodsList").bind("click", function(e) {
            e.preventDefault();
            
            //판매종료 여부체크
            var allSoldOutChk = $(this).find(".img").children("span").hasClass("allsoldOut");
            
            if(allSoldOutChk == false){
                common.link.moveGoodsDetailBarcode($(this).attr("data-ref-itemNo"));
            }
        });

        $(classNm + ".goodsListLogin").bind("click", function(e) {
            e.preventDefault();
            //로그인 성인체크 로그인성인
            common.link.moveGoodsDetailBarcode($(this).attr("data-ref-itemNo"));
        });

        $(classNm + ".goodsListAuth").bind("click", function(e) {
            e.preventDefault();
            //로그인 성인체크
            common.link.moveGoodsDetailBarcode($(this).attr("data-ref-itemNo"));
        });
    },
    
    /** 상품평 삭제 전 처리 **/
    moveGoodsGdasDel : function(gdasSeq, goodsNo, pntPayYn, retUrl){
        
        var loginCheck = common.loginChk();
        
//        var deleteMessage = '작성하신 상품평을 삭제하시겠습니까?';
        var deleteMessage = '작성한 리뷰를 삭제하시겠습니까?';
        
        if(!loginCheck) return;
        
        if(!confirm(deleteMessage)) return;
        
        common.delGdasProceess(gdasSeq, goodsNo, retUrl);
    },
    
    /** 상품평 삭제 처리 **/
    delGdasProceess : function(gdasSeq, goodsNo, retUrl){
            
        var data = {gdasSeq : gdasSeq, goodsNo:goodsNo, retUrl: retUrl};
        //jsTemplet
        common.Ajax.sendJSONRequest("POST"
            , _baseUrl + "mypage/delGdasJson.do?gdasSeq="+gdasSeq
            , data
            , common.afterDelGoodsGdasSuccess
        );
    },
    
    /** 상품평 삭제 후 처리 **/
    afterDelGoodsGdasSuccess : function(data){
        
        if(data.resultCd == "000"){
//            alert("성공적으로 삭제하였습니다.");
        	alert("삭제 완료");
            
            if ( data.retUrl == ""){
                location.href = _baseUrl + '/index.do';
            }else{
                location.href = data.retUrl;    
            }
        }else{
            alert("삭제가 실패하였습니다.");
        }
    },
    
    /** 개인정보취급방침 **/
    getPrivacy :function(){
        
    },
    
    /** 상품 사이즈별 이미지경로 생성 **/
    getSizeImgUrl : function(size, imgUrl){
        
        var goodsPath = "",
            imgSctCd = "",
            sizePath = "",
            prefixPath = _goodsImgUploadUrl;
        
        var pathArr = imgUrl.split("/");
        
        //  이미지 구분 코드를 구별하기 위한 Split
        if ( pathArr.length > 1 ){
            imgSctCd = pathArr[0];
        }
        
        //  상세 이미지일 경우 사이즈 패스 필요 없음
        if ( imgSctCd != "40"){
            sizePath = size + "/";
        }
        
        //  imgUrl + sizePath + goodsImgUrl 형식의 타입으로 Return
        //  상세 sizePath가 ""로 들어감
        goodsPath = prefixPath + sizePath + imgUrl;
        
        return goodsPath;
    },
    
    //  드래그 이벤트 막기
    preventDrag : function(){
        $('.userSelectnone').css('use-select','none');
    },
    
    //  행사안내 팝업 닫기
    closePromEvtPop : function(){
        
        common.popFullClose();
        
        if ( location.href.indexOf("getCart.do") > 0 && common.cart.regCartCnt > 0 ){
            var linkCartNo = location.href.substring(location.href.indexOf("cartNo=")+7);
            var sumCartNo = "";
            
            if(linkCartNo != ""){
                if(cartNosForDirectCart != ""){
                    sumCartNo = linkCartNo + "," + cartNosForDirectCart;
                }else{
                    sumCartNo = linkCartNo;
                }
            }
            
            cartNosForDirectCart = "";
            common.cart.regCartCnt = 0;
            
            if(location.href.indexOf("cartNo=") > 0){
                location.href = _secureUrl + "cart/getCart.do?cartNo=" + sumCartNo;
            }else{
                window.location.reload();
            }
        }
    },
    
    /**
     * 이미지 공통 처리
     */
    errorImg : function(obj) {
        obj.src = _imgUrl + "/comm/noimg_550.gif";
        obj.onerror = '';
    },
    
    errorProfileImg : function(obj) {
        obj.src = _imgUrl + "/comm/my_picture_base.jpg";
        obj.onerror = '';
    },
    
    errorBrandImg : function(obj) {
        obj.src = _imgUrl + "/comm/noimage_brandshop.png";
        obj.onerror = '';
    },
    resizeImg : function(obj, thum_size){
    	var _thisImg = $(obj);	
    	var _thisImgUrl = _thisImg.attr('src');
    	var _thiswidth = _thisImg.width(), _thisHeight = _thisImg.height();
    	var _resize_size = thum_size, _resize_ratio = 0;
    	var rs_width = 0, rs_height = 0, cs_width = 0, cs_height = 0;
    	var return_url = '';
    	if(_thiswidth > _thisHeight){
    		if(_thisHeight > _resize_size){
    			_resize_ratio = _resize_size / _thisHeight;
    			rs_width = _thiswidth * _resize_ratio, rs_height = _resize_size;
    			cs_width = _resize_size, cs_height = _resize_size;
    		}else if(_thisHeight < _resize_size){
    			rs_width = _thiswidth, rs_height = _thisHeight;
    			cs_width = _thisHeight, cs_height = _thisHeight;
    		}
    	}else if(_thiswidth < _thisHeight){
    		if(_thiswidth > _resize_size){
    			_resize_ratio = _resize_size / _thiswidth;
    			rs_width = _resize_size, rs_height = _thisHeight * _resize_ratio;
    			cs_width = _resize_size, cs_height = _resize_size;
    		}else if(_thiswidth < _resize_size){
    			rs_width = _thiswidth, rs_height = _thisHeight;
    			cs_width = _thiswidth, cs_height = _thiswidth;
    		}
    	}else if(_thiswidth == _thisHeight){
    		if(_thiswidth > _resize_size){
    			rs_width = _resize_size, rs_height = _resize_size;
    			cs_width = _resize_size, cs_height = _resize_size;
    		}else if(_thiswidth < _resize_size){
    			rs_width = _thiswidth, rs_height =_thiswidth;
    			cs_width = _thiswidth, cs_height = _thiswidth;
    		}
    	}
		return_url = _thisImgUrl+'?RS='+parseInt(rs_width)+'x'+parseInt(rs_height)+'&CS='+parseInt(cs_width)+'x'+parseInt(cs_height);
    	return return_url;
    	
    },
    onLoadProfileImg : function(obj, size){
    	var _thisProfileImg = $(obj);
    	if($(obj)[0].src.indexOf("?CS=") > -1){
    		var temp = $(obj)[0].src.substring(0,$(obj)[0].src.indexOf("?CS="));
    		$(obj)[0].src = temp;
    	}
    	_thisProfileImg.siblings('div.thum').find('img.profileThum_s').attr('src',common.resizeImg(obj, size));
    },
//    profileImgResize :  function(obj, thum_size){
//        var _thisImg = $(obj);  
//        var _thisImgUrl = _thisImg.attr('src');
//        var _thisWidth = _thisImg.width(), _thisHeight = _thisImg.height();
//        var _resize_size = thum_size, _resize_ratio = 0;
//        var rs_width = 0, rs_height = 0, cs_width = 0, cs_height = 0;
//        
//        _resize_ratio = _resize_size * 2
//        rs_width = _resize_ratio;
//        cs_width = _resize_size, cs_height = _resize_size;
//        
//        return_url = _thisImgUrl+'?RS='+parseInt(rs_width)+'x0'+'&CS='+parseInt(cs_width)+'x'+parseInt(cs_height);
//        return return_url;
//    },
    /** EP 쿠폰 오픈 **/
    epCouponOpen : function(){
        $("#layerPop").html($("#epCouponLayerPop").html());
        common.popLayerOpenNoReSize("LAYERPOP01");
    },
    
    /**
     * 웹로그 
     */
    wlog : function(wlKey) {
        setTimeout(function() {
            try {
                try{
                    if(wlKey=="goods_cart"){
                        n_click_logging(_baseUrl + "cart/regCartJson.do", _baseUrl + "goods/getGoodsDetail.do");
                    }else if(wlKey=="goods_buy"){
                        n_click_logging(_baseUrl + "order/getOrderForm.do", _baseUrl + "goods/getGoodsDetail.do");
                    }else if(wlKey=="home_curation_area"){
                        n_click_logging(_baseUrl + "?clickarea=" + wlKey);
                    }
                }catch(e){}
                n_click_logging(_baseUrl + "?clickspace=" + wlKey);
            } catch (e) {
            }
        }, 10);
        
//        setTimeout(function() {
//            $.ajax({
//                type: "GET",
//                url: _baseUrl + "common/dummyJson.do?wl_log=" + wlKey,
//                contentType: "application/json;charset=UTF-8",
//                dataType : 'json',
//                async: true,
//                cache: false,
//                success: function(data) {
//                }
//            });
//        }, 10);
    },
    
    //정상적인 상태에서 세션스토리지 정보를 리셋함.
    resetSessionStorage : function() {
        var popInfo = sessionStorage.getItem("urgentNoticePop");
        var bannerCloseYn = sessionStorage.getItem("bannerCloseYn");
        var subDispCatNo = sessionStorage.getItem("subDispCatNo");
        
        sessionStorage.clear();
        
        if (popInfo != undefined && popInfo != "undefined") {
            sessionStorage.setItem("urgentNoticePop", popInfo);
        }
        if (bannerCloseYn != undefined && bannerCloseYn != "undefined") {
            sessionStorage.setItem("bannerCloseYn", bannerCloseYn);
        }
        if (subDispCatNo != undefined && subDispCatNo != "undefined") {
            sessionStorage.setItem("subDispCatNo", subDispCatNo);
        }
    }
};


$.namespace("common.header");
common.header = {
     
    bindFlag : "N",
    scpClickFlag : "N",
    
    init : function() {
        common.header.bindEvent();
        if ( common.app.appInfo.isapp ){
            $(".mAllSearch").addClass("app");
        }
    },

    bindEvent : function() {
            $(".mAllSearch").find(".m_btn_barcode").bind("click", function() {
                location.href = "oliveyoungapp://scanBarcode"; 
                common.wlog("header_barcode_btn");
             });
            $(".searchScp").bind("click", function() {
                if(common.header.scpClickFlag=="N"){
                    common.header.getScpGoodsListAjax();
                    suddenKeyword();
                    getMyKeyword();
                    common.header.scpClickFlag="Y";
                }
            });
        },
    appDownBannerMainInit : function () {
        //배너를 종료여부
        if(sessionStorage.getItem("bannerCloseYn")!="Y"){
            //유입채널이 있으면
            //if($("#chlNoBanner")!=undefined && $("#chlNoBanner")!="" && $("#chlNoBanner")!=null && $("#chlNoBanner").val().length>0){
                //앱이 아니면
                if (common.app.appInfo == undefined || !common.app.appInfo.isapp) {
                    $('#webBanner_main').css("display","block");
                }
            //}
        }
        
        setTimeout(function() {
        	//앱다운로드 배너 추가건
            /* BI Renewal. 20190918. nobbyjin. Start
        	if($('#webBanner_main').css("display") == 'block'){
        	    $('#webBanner_main').parent().css('height', '190px');
        	    $('#mHome-visual').css({'padding-top':'48px'});
        	}else if($('#webBanner_main').css("display") == 'none'){   
        	    $('#webBanner_main').parent().css('height', '142px');
        	    $('#mHome-visual').css({'padding-top':'0'});
        	}
            BI Renewal. 20190918. nobbyjin. End */
            if($('#webBanner_main').css("display") == 'block'){
                $('#mContents').css({'padding-top':'48px'});
            }else if($('#webBanner_main').css("display") == 'none'){   
                $('#mContents').css({'padding-top':'0'});
            }
        },200);
        common.header.appDownBannerMainBindEvent();
    },
   
    appDownBannerMainBindEvent : function () {
        if(common.header.bindFlag=="N"){
            $("#banner_close_main").bind("click", function() {
                common.wlog("appDownClose");
                $('#webBanner_main').css('display','none');
                $('#webBanner_main').parent().css('height', '142px');
            	//BI Renewal. 20190918. nobbyjin. Start
                //$('#mHome-visual').css({'padding-top':'0'});
                $('#mContents').css({'padding-top':'0'});
            	//BI Renewal. 20190918. nobbyjin. End
                sessionStorage.setItem("bannerCloseYn","Y");
                
            });
            $("#hd_banner_cont_main").bind("click", function() {
                common.header.appDownLink();
            });
            
        }
        common.header.bindFlag="Y";
        
    },
    getScpGoodsListAjax: function(){
        var url= _baseUrl + "common/getScpGoodsListAjax.do";
        var loginResult = false;

        $.ajax({
            type: "POST",
            url: url,
            data: null,
            async: false,
            cache: false,
            success: function(data) {
                common.header.getScpGoodsListAjaxCallBack(data);
            },
            error : function() {
                
            }
        });
    },
    getScpGoodsListAjaxCallBack: function(data){
        
        $("#scp_cont_id").html(data);
        common.gnb.initScpSwiper();
       
    },
    appDownBannerInit : function () {
        //배너를 종료여부
        if(sessionStorage.getItem("bannerCloseYn")!="Y"){
            //유입채널정보가 있으면
            //if($("#chlNoBanner")!=undefined && $("#chlNoBanner")!="" && $("#chlNoBanner")!=null && $("#chlNoBanner").val().length>0){
                //앱이 아니면
                if (common.app.appInfo == undefined || !common.app.appInfo.isapp) {
                    $('#webBanner_detail').css("display","block");
                }
            //}
        }
            
        if($('#webBanner_detail').css("display") == 'block'){
            $('#mHeader.subHead.detail').css("height","auto");
            $('#mHgroup').css("height","auto");
        }else if($('#webBanner_detail').css("display") == 'none'){   
            $('#mHeader.subHead.detail').css("height","60px");
            //BI Renewal. 20190918. nobbyjin.
            //$('#mHgroup').css("height","97px");
        }
        common.header.appDownBannerBindEvent();
    },
    appDownBannerBindEvent : function () {
        $("#banner_close_detail").bind("click", function() {
            $('#webBanner_detail').css('display','none');
            $('#mHeader.subHead.detail').css("height","60px");
            //BI Renewal. 20190918. nobbyjin.
            //$('#mHgroup').css("height","97px");
            sessionStorage.setItem("bannerCloseYn","Y");
            common.wlog("appDownClose");
        });
        $("#hd_banner_cont_detail").bind("click", function() {
            common.header.appDownLink();
        });
    },
    
    appDownLink : function (){
        
        common.wlog("appDownLink");
        //var linkUrl = window.location.href.substring(window.location.href.indexOf("/m/")+3,window.location.href.length);
        //BI Renewal. 20190918. nobbyjin. - Link 수정
        //var linkUrl = "main/getCouponList.do?tabIdx=0&couponMode=coupon"
        var linkUrl = "main/getCouponList.do";
        common.link.commonMoveUrl("common/getAppDownload.do?redirectUrl="+linkUrl);
        
        
    }    
};
        
$.namespace("common.gnb");
common.gnb = {
    hasExe : false,
    
    init : function() {
        /*
        BI Renewal. 20190918. nobbyjin. Start.
        if (!common.gnb.hasExe && $(".allmenuOpen").length > 0) {
            common.gnb.bindEvent();

            setTimeout(function() {
                common.gnb.initSearchPop();
            }, 600);
            
//            setTimeout(function() {
                common.gnb.initSlideMenu();
//            }, 100);
                
                
            common.gnb.hasExe = true;
        }
        BI Renewal. 20190918. nobbyjin. End.
        */
        if (!common.gnb.hasExe && $("#footerTabCategoy").length > 0) {
            common.gnb.bindEvent();
            setTimeout(function() {
                common.gnb.initSearchPop();
            }, 600);
            common.gnb.initSlideMenu();
            common.gnb.hasExe = true;
        }
    },

    bindEvent : function() {
        /*
        BI Renewal. 20190918. nobbyjin. Start.
        $(".allmenuOpen").click(function(){
            //타이틀영역 숨기기
            $("#titConts").hide();
            
            var num=$(".allmenu").hasClass("show");
            
            if(!num){
                common.setLazyLoad();
                $(".allmenu").addClass("show").stop().animate({left: 0},240).parents("body").css({"overflow" : "hidden"})
                $(".dim").show();
                //앱 호출
                setTimeout(function() {
                    common.app.callMenu("Y");
                },100);
            }else {
                $(".allmenu").removeClass("show").stop().animate({left: '-85%'},200).parents("body").css({"overflow" : "visible"})
                $(".dim").hide();
                //앱 호출
                setTimeout(function() {
                    common.app.callMenu("N");
                },100);
            }
        });
        BI Renewal. 20190918. nobbyjin. End.
        */
        $(document).on('click', '.dim', function() {
            //타이틀영역 보이기
            $("#titConts").show();
            
            if($(".allmenu").hasClass("show") || $("#footerTab").length > 0){
                try{
                    //BI Renewal. 20190918. nobbyjin.
                    $("#footerTab").show();
                    
                    var scollH = $(".allmenu div").scrollTop();
                    if (scollH>0){
                        $(".allmenu div").scrollTop(0.0);
                    }
                    $(".allmenu").stop().animate({left: '-100%'},200).delay(2000).parents("body").css({"overflow" : "visible"});
                    setTimeout(function() {
                        $(".allmenu").removeClass("show");
                     }, 150);
                    $(".dim").hide();
                    //앱 호출
                    setTimeout(function() {
                        common.app.callMenu("N");
                    }, 100);
                }catch(e){}

            } else if($(".popLayerWrap").hide()){//추가부분
                $("body").css({"overflow" : "visible"});
                try{
                    var varNowScrollY = parseInt(sessionStorage.getItem("scrollY_popLayer"));

                    if((varNowScrollY > 0)){
                        $(window).scrollTop(varNowScrollY);
                        sessionStorage.removeItem("scrollY_popLayer");
                    }
                }catch(e){}

                $('.dim').hide();
            }
        });

        //탭 컨텐츠 show/hide
        $('#mTab').find('button').on({
            'click' : function(e){
                e.preventDefault();
                $(this).parent().addClass('on').attr('title', '현재 선택된 메뉴').siblings().removeClass('on').removeAttr('title');
                $('.tab_contents:eq('+ $(this).parent().index() +')').removeClass('hide').siblings().addClass('hide');
            }
        });

        //장바구니 버튼
        $("#mHeader").find(".mBasket").click(function() {
            common.link.moveCartPage();
        });
        
        $(".mAllSearch").find("button").click(function() {
            if ($(this).attr("data-ref-linkurl") != undefined && $(this).attr("data-ref-linkurl") != "") {
                location.href = $(this).attr("data-ref-linkurl");
            }
        });
        
   
      //검색 버튼
        if ($("#mContainer").find(".mAllSearch a").length > 0) {
            $("#mContainer").find(".mAllSearch a").click(function() {
                history.replaceState({status:"search", scrollY:common.getNowScroll().Y}, null, null);

                $("#mWrapper").hide();
                $("#mSearchWrapper").show();
                $(document).scrollTop(0);
                
                $("#mSearchWrapper").find("#query").focus();
                
                history.pushState({status:"SearchPop"},null,null);
                common.wlog("detailsrch_input");
                //common.gnb.initScpSwiper();
            });
        }
        
        //검색 버튼
        $("#mHeader").find(".mAllSearch a").click(function() {
            history.replaceState({status:"search", scrollY:common.getNowScroll().Y}, null, null);
            
            $("#mWrapper").hide();
            $("#mSearchWrapper").show();
            $(document).scrollTop(0);

            $("#mSearchWrapper").find("#query").focus();

            history.pushState({status:"SearchPop"},null,null);
            common.wlog("detailsrch_input");
            //common.gnb.initScpSwiper();
        });

        $("#mHeader").find(".mSearch").click(function() {
            history.replaceState({status:"search", scrollY:common.getNowScroll().Y}, null, null);

            
            $("#mWrapper").hide();
            $("#mSearchWrapper").show();
            $(document).scrollTop(0);
            
            $("#mSearchWrapper").find("#query").focus();

            history.pushState({status:"SearchPop"},null,null);
            common.wlog("detailsrch_input");
            //common.gnb.initScpSwiper();
            if(common.header.scpClickFlag=="N"){
                common.header.getScpGoodsListAjax();
                suddenKeyword();
                getMyKeyword();
                common.header.scpClickFlag="Y";
            }
        }); 
        
        $('#fixedSearch  input').click(function() {            
            history.replaceState({status:"search", scrollY:common.getNowScroll().Y}, null, null);

            $("#mWrapper").hide();
            $("#mSearchWrapper").show();
            $(document).scrollTop(0);
            
            $("#mSearchWrapper").find("#query").focus();

            history.pushState({status:"SearchPop"},null,null);
            common.wlog("detailsrch_input");
            //common.gnb.initScpSwiper();
        });
        
        //BI Renewal. 20190918. nobbyjin. Start.
        //유틸바 카테고리
        $("#footerTabCategoy").click(function() {
            //타이틀영역 숨기기
            $("#titConts").hide();
            
            var num=$(".allmenu").hasClass("show");
            common.wlog("mFooterTabCategoy");
            
            if(!num){
                common.setLazyLoad();
                $(".allmenu").addClass("show").stop().animate({left: 0},240).parents("body").css({"overflow" : "hidden"});
                $(".dim").show();
                //앱 호출
                setTimeout(function() {
                    common.app.callMenu("Y");
                },100);
                
                if ( common.app.appInfo.isapp ){
                    // 앱일 경우 새로운 푸시 있는지 확인하는 스킴 호출
                    location.href = "oliveyoungapp://getpushnewmsgyn";
                }
            }else {
                $(".allmenu").removeClass("show").stop().animate({left: '-85%'},200).parents("body").css({"overflow" : "visible"});
                $(".dim").hide();
                //앱 호출
                setTimeout(function() {
                    common.app.callMenu("N");
                },100);
            }
        });
        
        //유틸바 검색
        $("#footerTabSearch").click(function() {
            history.replaceState({status:"search", scrollY:common.getNowScroll().Y}, null, null);
            
            $("#mWrapper").hide();
            $("#mSearchWrapper").show();
            $(document).scrollTop(0);

            $("#mSearchWrapper").find("#query").focus();

            history.pushState({status:"SearchPop"},null,null);
            common.wlog("mFooterTabSearch");
            //common.gnb.initScpSwiper();
            if(common.header.scpClickFlag=="N"){
                common.header.getScpGoodsListAjax();
                suddenKeyword();
                getMyKeyword();
                common.header.scpClickFlag="Y";
            }
        });
        
        //유틸바 홈
        $("#footerTabHome").click(function() {
            common.wlog("mFooterTabHome");
            common.link.moveMainHome();
        });
        
        //유틸바 마이페이지
        $("#footerTabMypage").click(function() {
            common.wlog("mFooterTabMypage");
            common.link.moveMyPageMain();
        });
        
        //유틸바 최근 본 상품
        $("#footerTabRecentGoods").click(function() {
            common.wlog("mFooterTabRecentGoods");
            common.link.moveRecentList();
        });
        //BI Renewal. 20190918. nobbyjin. End.
        
        $(window).bind("popstate", function() {

            //팝업 닫는시점인 경우
            if (history.state != null && history.state.status == "search") {
                $("#mSearchWrapper").hide();
                $("#mWrapper").show();
                setTimeout(function() {
                    var _mainCheck = $('#mHome-visual');
                    var _btnPlay = $('#mHome-visual').find('#swiper-autoplay').attr('class');
                    $(document).scrollTop(1);
                    $(document).scrollTop(history.state.scrollY);
                    $(document).scroll();                    
                    if(_mainCheck.length>0){
                        if(_btnPlay){
                            var _dot = $('#mHome-visual').find('.control_box .paging .swiper-pagination-bullet-active');                        
                            _dot.next('span').click();
                        }
                    }                   
                }, 100);
            }

        });

        //웹로그 바인드
        setTimeout(function() {
            common.gnb.bindWebLog();
        }, 200);

        
    },

    initSlideMenu : function() {
        //drawer메뉴 ajax처리하지 않아 다음 코드 주석처리.
        //5분 세션 저장 timeout 시작
        var connectedTime = "";
        var status = false;
        try{
            connectedTime = sessionStorage.getItem("slideSavedTime");
            status = sessionStorage.getItem("slideStatus");
        }catch(e){}

        if (!common.isEmpty(connectedTime)) {
            var currentTime = (new Date()).getTime();
            var connectMin = Math.floor((currentTime - connectedTime) / (60 * 1000));

            if (connectMin > 5 || (status == "true") != _isLogin) {
                try {
                    sessionStorage.removeItem("slideMenu");
                }catch(e){}

            }
        } else {
            sessionStorage.removeItem("slideMenu");
        }

        //시간 업뎃
        //사파리 private mode 예외처리용
        try {
            sessionStorage.setItem("slideStatus", _isLogin);
        } catch (e) {}
        //5분 세션 저장 timeout 끝

      //슬라이드 html 조회
        var html;
        try {
            html = sessionStorage.getItem("slideMenu");
        }catch(e){}

        //화면 HTML 조회 처리
        if (!common.isEmpty(html)) {

            $("#allMenu #mLnb").html(html);

            setTimeout(function() {
                $('#footerTab').show();
            },600);
        } else {
            //저장된 화면이 없을 경우 html 조회
            common.gnb.callSlideMenuAjax();
        }

        //최근 본 상품
        setTimeout(function() {
            try {
                common.recentGoods.init();
            } catch (e) {}
        }, 300);

        setTimeout(function() {
            common.gnb.bindSlideMenu();
        }, 200);
    },
    
    callSlideMenuAjax : function() {
        // 앱 버전 체크 위해 파라미터 추가
        var data = {};
        if (common.app.appInfo.isapp) {
            var osType  = common.app.appInfo.ostype;
            var appVer = common.app.appInfo.appver.replace(/\./gi, "");
            data = {
                    osType : osType,
                    appVer : appVer,
            };
        }
        //저장된 화면이 없을 경우 html 조회
        $.ajax({
            type: "GET",
            url: _baseUrl + "common/slideMenuAjax.do",
            data: data,
            dataType : 'text',
            async: false,
            cache: false,
            success: function(data) {

                $("#allMenu #mLnb").html(data);
                
                //세션에 저장
                //사파리 private mode 예외처리용
                try {
                    sessionStorage.setItem("slideMenu", $("#allMenu #mLnb").html());
                    sessionStorage.setItem("slideSavedTime", (new Date()).getTime());
                } catch (e) {}
            },
            error: function() {
                
            }, complete: function(){
                setTimeout(function() {
                    $('#footerTab').show();
                }, 600);
            }
        });
    },

    bindSlideMenu : function() {
       $(".lnb_sec .lnb_dep3 li a").each(function() {
            $(this).bind("click", function() {
                common.app.callTrackEvent("category", {"af_content_nm" : $(this).children("span").text()});
                // left 메뉴 > 카테고리 > 서브카테고리 선택시 저장된 서브카테고리 제거
                sessionStorage.removeItem("subDispCatNo");
                
                var dispCatNo = $(this).attr("data-ref-dispCatNo");
                setTimeout(function() {
                    location.href = _baseUrl + "display/getSCategoryList.do?dispCatNo=" + dispCatNo;
                }, 300);
            });
        });
      

           $('.lnb_dep1 > li > a').click(function(){
               if ($(this).parents('li').hasClass('on')){
                  $(this).parents().find('li').removeClass('on'); 
               }else {
                   common.app.callTrackEvent("category", {"af_content_nm" : $(this).children("span").text()});
                   $(this).parents('li').addClass('on').siblings(".lnb_dep1 li").removeClass('on');
                   $('.lnb_dep2 li').removeClass('on');
               }
           });
           $('.lnb_dep2 > li > a').click(function(){
               if ($(this).parents('.lnb_dep2 li').hasClass('on')){
                   $(this).parents('.lnb_dep2 li').removeClass('on');
               }else {
                   common.app.callTrackEvent("category", {"af_content_nm" : $(this).children("span").text()});
                   $(this).parents('.lnb_dep2 li').addClass('on').siblings('.lnb_dep2 li').removeClass('on');
               }
           });


        //전문관 클릭 이벤트
        $(".lnb-jeonmungwan ul li a[data-ref-dispCatNo]").bind("click", function() {
                var dispCatNo = $(this).attr("data-ref-dispCatNo");
                common.link.moveSpcShop(dispCatNo);
        });
        
        //slideMenu Top 프리미엄관 new 클릭 이벤트
        $(".subLink .mlink a[data-ref-dispCatNo]").bind("click", function() {
                var dispCatNo = $(this).attr("data-ref-dispCatNo");
                common.link.moveSpcShop(dispCatNo);
        });

        $(".allmenuClose").click(function(){
            //타이틀영역 숨기기
            $("#titConts").show();
            
            var scollH = $(".allmenu div").scrollTop();
            if (scollH>0){
                $(".allmenu div").scrollTop(0.0)
            }
            $(".allmenu").stop().animate({left: '-100%'},200).delay(2000).parents("body").css({"overflow" : "visible"});
            setTimeout(function() {
                $(".allmenu").removeClass("show");
             }, 150);
            
            $(".dim").hide();
            
            //웹 접근성 popfocus 삭제 ( 2017-05-11 )
            $("a[data-focus~=on]").focus(); // 표시해둔 곳으로 초점 이동
            window.setTimeout(function(){
                $("a[data-focus~=on]").removeAttr("data-focus");
            },100); // 
            
            //앱 호출
            setTimeout(function() {
                common.app.callMenu("N");
            }, 100);
        });

       /* $('.lnbTab').find('li').on('click', function(){
            var idx = $(this).index();
            $('.lnbTab').find('li').removeClass('on').eq(idx).addClass('on');
            $('.lnbSmenu').find('.tab_contents').removeClass('on').eq(idx).addClass('on');
        });*/
        
        //웹로그 바인드
        setTimeout(function() {
            common.gnb.bindWebLog("slide");
        }, 200);


    },
    
    
    initSearchPop : function() {
    //저장된 화면이 없을 경우 html 조회
        $.ajax({
            type: "GET",
            url: _baseUrl + "common/slideSearchAjax.do",
            data: null,
            dataType : 'text',
            async: false,
            cache: false,
            success: function(data) {

                $("#mSearchWrapper").html(data);

                //세션에 저장
                //사파리 private mode 예외처리용
                try {
                    sessionStorage.setItem("searchPopMenu", $("#mSearchWrapper").html());
                } catch (e) {}
            },
            error: function() {

            }
        });

        setTimeout(function() {
            common.gnb.bindSearchPop();
        }, 200);
        
        try {
            ark_init();
            //popkeyword();
            //suddenKeyword();
            //getMyKeyword();
            
            $("#mSearchWrapper").find(".search_tab_area").find(".tab_contents").show();
            
            var myKeyword = getCookie_search("mykeyword");
            
            if (myKeyword == undefined || myKeyword.trim() == "") {
                $("#mSearchWrapper").find('#mTab11 > li').removeClass("on");
                $("#mSearchWrapper").find('#mTab11 > li:eq(1)').addClass("on");
                
                $("#mSearchWrapper").find('.tab_contents:eq(1)').removeClass('hide').siblings('.tab_contents').addClass('hide');

            }

        } catch(e) {
            console.log(e);
        }
    },

    bindSearchPop : function() {
        $("#mSearchWrapper").find("#schBackBtn").bind("click", function() {
            history.back();
        });
        
        $("#mSearchWrapper").find(".btn_sch").bind("click", function() {
           if($("#mSearchWrapper").find("#query").val()=='') return;
           
           var giftYn = common._giftCardCheck($("#mSearchWrapper").find("#query").val());
           
           location.href = _baseUrl + "search/getSearchMain.do?query=" + encodeURIComponent($("#mSearchWrapper").find("#query").val())+"&giftYn=" + giftYn; 
        });
        
        $("#mSearchWrapper").find(".btn_sch_barcode").bind("click", function() {
            location.href = "oliveyoungapp://scanBarcode"; 
            common.wlog("detailsrch_barcode_btn");
         });
        
        $(".searchScpInput").bind("click", function() {
            if(common.header.scpClickFlag=="N"){
                common.header.getScpGoodsListAjax();
                suddenKeyword();
                getMyKeyword();
                common.header.scpClickFlag="Y";
            }
        });
  
        /**
         * 퍼블 작성 스크립트
         */
        $("#mSearchWrapper").find('#mTab11 > li').find('a').on({
            'click' : function(e){
                e.preventDefault();
                $(this).parent().addClass('on').attr('title', '현재 선택된 메뉴').siblings().removeClass('on').removeAttr('title');
                $("#mSearchWrapper").find('.tab_contents:eq('+ $(this).parent().index() +')').removeClass('hide').siblings('.tab_contents').addClass('hide');
            }
        });
        $("#mSearchWrapper").find('.tabList01 > li').each(function(){
            if(!$(this).hasClass('on')){
                $("#mSearchWrapper").find('.tab_contents:eq('+ $(this).index() +')').addClass('hide');
            }
        });

        $("#mSearchWrapper").find('.sch_field2').find('.btn_sch_del').on({
            'click' : function(e){
                e.preventDefault();
                $(this).removeClass('on').parent().find('input[type="text"]').val('').focus();
                var _input = $(this).parent().find('input[type="text"]');
                fnSearchSet(_input);
                $("#query").keyup();
            }
        });
        $("#mSearchWrapper").find('.sch_field2').find('input[type="text"]').on({
            'keyup' : function(){
                fnSearchSet($(this));
            },
            'focusin' : function(){
                fnSearchSet($(this));
            }
        })

        function fnSearchSet(obj){
            if(obj.val() != '' && obj.val() != null){       //페이지 확인을 위한 스크립트 처리(개발시 삭제하셔도됩니다)
                $("#mSearchWrapper").find('.search_auto_area').removeClass('off');
                $("#mSearchWrapper").find('.search_tab_area').addClass('off');
                obj.parent().find('.btn_sch_del').addClass('on');//2017-01-11 추가
                $("#mSearchWrapper").find('#mContainer').addClass('fix');
                $("#mSearchWrapper").find('#mTab11').hide();
                doArk(obj.val());
                request_M_ArkJson(obj.val());
            }
            else{
                $("#mSearchWrapper").find('.search_auto_area').addClass('off');
                $("#mSearchWrapper").find('.search_tab_area').removeClass('off');
                obj.parent().find('.btn_sch_del').removeClass('on');
                $("#mSearchWrapper").find('#mContainer').removeClass('fix');
                $("#mSearchWrapper").find('#mTab11').show();

            }
        }
        /**
         * 퍼블 작성 스크립트 - 끝
         */
        
        //웹로그 바인드
        setTimeout(function() {
            common.gnb.bindWebLog("search");
        }, 200);

    },
    
    //웹로그 바인딩
    bindWebLog : function(type) {
        
        if (type == undefined || type == null || type == "") {
            //올리브영 BI
            $("#mHgroup h1 a").bind("click", function() {
                common.wlog("home_header_bi");
            });
            //햄버거메뉴
            $(".allmenuOpen").bind("click", function() {
                common.wlog("home_header_drawer");
            });
            //장바구니 버튼
            $(".mBasket .basket").bind("click", function() {
                common.wlog("home_header_cart");
            });

            //footer 영역
            $("#login, #logout").bind("click", function() {
                common.wlog("home_footer_login");
             });
            $("#customer").bind("click", function() {
                common.wlog("home_footer_customer");
             });
            $("#onlinePhone").bind("click", function() {
                common.wlog("home_footer_onlinephone");
             });
            $("#1to1").bind("click", function() {
                common.wlog("home_footer_1to1");
             });
            $("#storePhone").bind("click", function() {
                common.wlog("home_footer_storephone");
             });
            $("#email").bind("click", function() {
                common.wlog("home_footer_email");
             });
            $(".mlist-sns .facebook").bind("click", function() {
                common.wlog("home_footer_sns_facebook");
             });
            $(".mlist-sns .twitter").bind("click", function() {
                common.wlog("home_footer_sns_twitter");
             });
            $(".mlist-sns .kakao").bind("click", function() {
                common.wlog("home_footer_sns_kakao");
             });
            $(".mlist-sns .ribyoung").bind("click", function() {
                common.wlog("home_footer_sns_navercafe");
             });
            $(".mlist-sns .instagram").bind("click", function() {
                common.wlog("home_footer_sns_instagram");
             });
        }

        if (type == "search") {
            //검색
            $("#mSearchWrapper #mTab11 li:eq(0)").bind("click", function() {
                common.wlog("home_header_search_recent");
            });
            //검색
            $("#mSearchWrapper #mTab11 li:eq(1)").bind("click", function() {
                common.wlog("home_header_search_populoar");
            });
        }
        
        
        if (type == "slide") {
            //로그인
            $(".linkLogin").bind("click", function() {
                common.wlog("home_drawer_login");
            });
            //설정
            $(".intro .setup").bind("click", function() {
                common.wlog("home_drawer_setting");
            });
            //알림
            $(".intro .notice.new").bind("click", function() {
                common.wlog("home_drawer_alarm");
            });
            //마이페이지
            $(".shortcuts .mypage").bind("click", function() {
                common.wlog("home_drawer_mypage");
            });
            //주문배송
            $(".shortcuts .order").bind("click", function() {
                common.wlog("home_drawer_delivery");
            });
            //매장안내
            $(".shortcuts .burial").bind("click", function() {
                common.wlog("home_drawer_store");
            });
            //고객센터
            $(".shortcuts .customer").bind("click", function() {
                common.wlog("home_drawer_customer");
            });
            //관심매장 로그인
            $(".notice-list .notice-type button.login").bind("click", function() {
                common.wlog("home_drawer_interest_login");
            });
            //관심매장 공지
            $(".notice-list .storeNtcList").bind("click", function() {
                common.wlog("home_drawer_interestnews_" + $(this).attr("data-ref-ntcSeq"));
            });
            //공지사항
            $(".notice-area a").bind("click", function() {
                common.wlog("home_drawer_notice");
            });
/*
            //뷰티
            $(".lnbSmenu .lnbTab li:eq(0)").bind("click", function() {
                common.wlog("home_drawer_beauty");
            });
            //푸드
            $(".lnbSmenu .lnbTab li:eq(1)").bind("click", function() {
                common.wlog("home_drawer_food");
            });
            //라이프탭
            $(".lnbSmenu .lnbTab li:eq(2)").bind("click", function() {
                common.wlog("home_drawer_life");
            });
*/
            //전문관
            $(".lnb-jeonmungwan li").each(function(idx) {
                $(this).find("a").bind("click", function() {
                    common.wlog("home_drawer_special_menu" + (idx + 1));
                });  
            });
            //최근본상품에 대한 웹로그는 common.recentGoods에서 처리함.
            //최근본상품 더보기
            $(".late-conts a.btnMore").bind("click", function() {
                common.wlog("home_drawer_recent_more");
            });
            //하위 카테고리
            $(".lnb_sec li a").bind("click", function() {
                common.wlog("home_drawer_category_" + $(this).attr("data-ref-dispCatNo"));
            });
        }
    },
    initScpCnt : 0,
    //SCP상품 swiper init
    initScpSwiper : function() {
        if(common.gnb.initScpCnt==0){
            // SCP
            //랜덤 숫자 가져오는 함수
            var fnRandomNum = function(max){            //랜덤 숫자 가져오는 함수(0~max사이의 값)
             return Math.floor(Math.random() * max);
            }
            
            var eventFull_len = $('.scp_slide .swiper-wrapper').children('li').length;       //FULL 배너 slide 갯수
            var eventFull_init_no = fnRandomNum(eventFull_len);                  //FULL 배너 slide  초기번호 
            
            //이벤트 FULL 배너 slide
            var scpSlide_set = {
             slidesPerView: 1,
             initialSlide : 0,
             autoplay: false,
             pagination: '.paging',
             nextButton: '.next',
             prevButton: '.prev',
             autoplayDisableOnInteraction: true,
             paginationClickable: true,
             freeMode: false,
             spaceBetween: 0,
             loop: false
            }, visual_swiper = Swiper('.scp_slide', scpSlide_set );
            common.gnb.initScpCnt=1;
            
            $(".swiper-slide.move-goods").bind("click", function() {
                
                common.wlog("search_scp_idx_"+visual_swiper.clickedIndex);
                var goodsNo = $(this).find("input[name*='goodsNo']").val();
                common.link.moveGoodsDetail(goodsNo);
            });
            
        }
    }
};

$.namespace("common.list");
common.list = {
    /**
     * 리스트의 checkbox 클릭 이벤트 바인딩
     * : 전체선택, 일부 선택에 대한 checkbox 선택 동작 이벤트
     *
     * 1. 전체선택 선택/해제 시 리스트에 있는 체크박스 모두가 선택/해제 됨
     *
     * 2. 목록의 체크박스 선택/해제시
     *      2.1 모두 선택되었을 경우 전체선택 체크박스가 선택 됨.
     *      2.2 일부만 선택 되거나 모두 해제 되었을 경우 전체선택 체크박스가 해제 됨.
     *
     * 사용법 :
     *              // ---------------------------------------------------------
     *              // 리스트의 체크박스를 선택/해제 하는 이벤트 바인딩
     *              // ---------------------------------------------------------
     *              // >> 전체선택 체크박스 : id="inp_all"
     *              // >> 목록의  체크박스 : class="chkSmall"
     *              // ---------------------------------------------------------
     *              <table>
     *                  <tr>
     *                      <th><input type="checkbox" id="inp_all">헤더체크박스</th>
     *                      ...
     *                  </tr>
     *                  <tr>
     *                      <th><input type="checkbox" class="chkSmall">목록체크박스1</th>
     *                      ...
     *                  </tr>
     *                  <tr>
     *                      <th><input type="checkbox" class="chkSmall">목록체크박스2</th>
     *                      ...
     *                  </tr>
     *                  ...
     *              </table>
     *              common.list.bindListCheckboxEvent('#inp_all', '.chkSmall');
     *
     * @param allChkboxJqSelector
     *              "전체선택" 체크박스의 JQuery Selector
     * @param listChkboxJqSelector
     *              리스트에 있는 체크박스의 JQuery Selector
     *
     *
     */
    bindListCheckboxEvent : function(allChkboxJqSelector, listChkboxJqSelector) {
        // 전체선택 체크박스(id=inp_all) 클릭시 동작
        $(allChkboxJqSelector).change(function(){
            $(listChkboxJqSelector).prop('checked',this.checked)
        });
        // 목록 체크박스("class=chkSmall") 클릭 시 동작
        $(listChkboxJqSelector).click(function(){
            var uncheckedEa = $(listChkboxJqSelector + ':not(:checked)').length;
            $(allChkboxJqSelector).prop('checked', uncheckedEa==0);

        });
    }
};

/*--------------------------------------------------------------------------------*\
* SNS 공통 script
\*--------------------------------------------------------------------------------*/
$.namespace("common.sns");
common.sns = {

    /**
     * 공통 SNS 공유 방법
     *
     * 1. 공유를 원하는 페이지에서 common.sns.init 함수를 호출한다.
     *
     * 2. init 함수 호출에 필요한 인자는 이미지URL, 공유제목, 공유할 URL이 된다.
     *
     * 3. init후에는 각 공유 버튼 마다 common.sns,doShare 함수를 호출한다.
     *
     * 4. doShare에 필요한 호출인자는 공유할 서비스 명이다. (kakaotalk, kakaostory, facebook)
     *
     */

    imgUrl : '',        //  이미지 URL
    title : '',         //  공유 제목
    shareUrl : '',      //  공유할 URL
    
    imgUrlKakaoEvt : '',        //  이미지 URL
    titleKakaoEvt : '',         //  공유 제목
    shareUrlKakaoEvt : '',      //  공유할 URL

    //  SNS 공유 Init 함수(페이지 상단 공통 공유용)
    init : function(imgUrl, title, shareUrl) {

        //  카카오톡 Init 
        // 개인계정의 카카오톡 자바스크립트 앱키로 적용해야함
        Kakao.init('24077b12ac18b11a96696382ccaa7138');
//        Kakao.init('0305c586bcd3328a207f11633e65717a');

        common.sns.imgUrl = imgUrl;
        common.sns.title = '[Oliveyoung]' + title;
        common.sns.shareUrl = shareUrl;
        
        //  https-> http로 변경
        common.sns.imgUrl = common.sns.imgUrl.replaceAll("https://", "http://");
        common.sns.shareUrl = common.sns.shareUrl.replaceAll("https://", "http://");
        
        //  URL INPUT BOX 세팅
//        $("#shareUrlTxt").attr("value",shareUrl);
//        $(".input-url").html(shareUrl);
        $("#CopyUrl").text(shareUrl);
        $("#btnShare").css("display", "block");
    },

    //  페이스북 공유를 위한 메타 태그 세팅
    metaTagInit : function(){
        $("meta[property='og:title']").attr("content", common.sns.title);
        $("meta[property='og:url']").attr("content", common.sns.shareUrl);
        $("meta[property='og:image']").attr("content", common.sns.imgUrl);
    },

    //  공유 처리
    doShare : function(type, appYn) {
        
        //  메타 태그 INIT
        common.sns.metaTagInit();

        //  카카오톡
        if(type == "kakaotalk") {

            Kakao.Link.sendDefault({
                objectType: 'feed',
                content: {
                    title: common.sns.title,
                    imageUrl:  common.sns.imgUrl,
                    link: {
                        webUrl:common.sns.shareUrl, 
                        mobileWebUrl: common.sns.shareUrl,
                        androidExecParams: common.sns.shareUrl,
                        iosExecParams : common.sns.shareUrl
                    }
                },
                buttons: [
                    {
                      title: '앱으로 보기',
                      link: {
                        mobileWebUrl: common.sns.shareUrl,
                        webUrl:common.sns.shareUrl
                      }
                    }
                  ]
            });

        }else if(type == "kakaostory"){         //  카카오스토리
            Kakao.Story.open({
                url: common.sns.shareUrl,
                text: common.sns.title
              });
        }else if(type == "facebook") {          //  페이스북

            var facebook_url = "";
            facebook_url += "http://m.facebook.com/sharer.php?";
            facebook_url += "u=" + common.sns.shareUrl;

            //  앱일경우와 앱이 아닐 경우
            if ( common.app.appInfo.isapp ){
                common.app.callOpenPage("페이스북", facebook_url,'','Y');
            }else{
                window.open(facebook_url);
            }
            
        }else if(type == "url"){
            
            if ( $("#SNSLAYER").find("#urlInfo").hasClass("on") ){
                $("#SNSLAYER").find("#urlInfo").hide();
                $("#SNSLAYER").find("#urlInfo").removeClass("on");
            }else{
                $("#SNSLAYER").find("#urlInfo").addClass("on");
                $("#SNSLAYER").find("#urlInfo").show();
//                $("#SNSLAYER").find("#shareUrlTxt").focus();
                $("#SNSLAYER").find("#CopyUrl").focus();
            }
        }
    },
    
    //  SNS 공유 Init 함수(카톡공유를 통한 이벤트용)
    initKakaoEvt : function(imgUrl, title, shareUrl) {

        common.sns.imgUrlKakaoEvt = imgUrl;
        common.sns.titleKakaoEvt = '[Oliveyoung]' + title;
        common.sns.shareUrlKakaoEvt = shareUrl;
        
        //  https-> http로 변경
        common.sns.imgUrlKakaoEvt = common.sns.imgUrlKakaoEvt.replaceAll("https://", "http://");
        common.sns.shareUrlKakaoEvt = common.sns.shareUrlKakaoEvt.replaceAll("https://", "http://");        
        
    },
    
    metaTagInitKakaoEvt : function(){
        $("meta[property='og:title']").attr("content", common.sns.titleKakaoEvt);
        $("meta[property='og:url']").attr("content", common.sns.shareUrlKakaoEvt);
        $("meta[property='og:image']").attr("content", common.sns.imgUrlKakaoEvt);
    },

//  공유 처리
    doShareKakaoEvt : function(type, appYn) {
        
        //  메타 태그 INIT
        common.sns.metaTagInitKakaoEvt();

        //  카카오톡
        if(type == "kakaotalk") {
            Kakao.Link.sendDefault({
                objectType: 'feed',
                content: {
                    title: common.sns.titleKakaoEvt,
                    imageUrl:  common.sns.imgUrlKakaoEvt,
                    link: {
                        webUrl:common.sns.shareUrlKakaoEvt, 
                        mobileWebUrl: common.sns.shareUrlKakaoEvt,
                        androidExecParams: common.sns.shareUrlKakaoEvt,
                        iosExecParams : common.sns.shareUrlKakaoEvt
                    }
                },
                buttons: [
                    {
                      title: '앱으로 보기',
                      link: {
                        mobileWebUrl: common.sns.shareUrlKakaoEvt,
                        webUrl:common.sns.shareUrlKakaoEvt
                      }
                    }
                  ]
            });

        }
    }
};


/*--------------------------------------------------------------------------------*\
* 장바구니 공통 script
\*--------------------------------------------------------------------------------*/
$.namespace("common.cart");
common.cart = {
        /* 레코벨 장바구니 담기 추가하면서 추가 S */
        cartNo : '',
        promKndCd : '',
        promNo : '',
        buyCnt : 0,
        getItemAutoAddYn : 'N',     // 프로모션 Get군 상품 자동증가 여부 (Get군의 상품 종류가 1가지일 경우)
        getItemGoodsNo : '',        // Get 상품 종류가 1가지인 goods_no
        getItemItemNo : '',             // Get 상품 종류가 1가지인 item_no
        /* 레코벨 장바구니 담기 추가하면서 추가 E */
        regCartCnt : 0,
        jsonParam : undefined,     
        
        cartSelValid : function(goodsNo, itemNo, goodsSctCd){
            var msg = "옵션을 선택해주시기 바랍니다.";
            
            if( goodsSctCd == "20"){
                msg = "상품을 선택해주시기 바랍니다.";
            }    
            
            if((goodsNo != undefined && goodsNo != "") || ( itemNo != undefined && itemNo != "" )){
                return true;
            }else{
                alert(msg);
                return false;
            }
        },
        
        // jwkim 오늘드림 옵션상품 개편후 mbrDlvpSeq 값 추가 
        regCart : function(cartSelGetInfoList, directYn, saveTp, listYn, optChgYn, cartYn, mbrDlvpSeq){
            
            var url = _baseUrl + "cart/regCartJson.do";

            var callBackResult = "";
            
            var quickYn = "N";
            
            var dlvpSeq = ""; // 오늘드림 배송지 선택한 배송seq 번호 jwkim
            
            var callback = function(data) {
                var result = data.result;
                callBackResult = data;
                
                if ( result ){
                    //장바구니 수량 업데이트
                    $.ajax({
                        type: "POST",
                        url: _baseUrl + "common/getCartCntJson.do",
                        contentType: "application/json;charset=UTF-8",
                        dataType : 'json',
                        async: false,
                        cache: false,
                        success: function(data) {                            
                            if(data != 0){
                                $("#mHeader .basket").append('<span class="cnt">');
                                $("#mHeader").find(".basket .cnt").css('display','block');
                                $("#mHeader").find(".basket .cnt").text(data);
                            }else{
                                $("#mHeader").find(".basket .cnt").css('display','none');
                            }   
                        }
                    });
                    
                    //바로구매시 사용할 return cartNo
                    if ( directYn == 'Y' ){
                        if( listYn == 'A' || listYn == 'S' )
                            return false;
                        
                        // 오늘드림 배송지 선택한 배송seq 번호 jwkim
                        if(mbrDlvpSeq != "" && mbrDlvpSeq != undefined){
                            dlvpSeq = mbrDlvpSeq;
                        }
                        
                        //  장바구니로 이동
                        setTimeout(function() {
                            // jwkim 오늘드림배송에서 일반으로 배송하는 로직에서 사용하는 mbrDlvpSeq(배송지seq) 값추가
                            location.href = _secureUrl + "cart/getCart.do?cartNo=" + data.rCartNo + "&quickYn=" + quickYn + "&mbrDlvpSeq=" + dlvpSeq;
                            //location.href = _secureUrl + "cart/getCart.do?cartNo=" + data.rCartNo + "&quickYn=" + quickYn; // as-is 소스
                        }, 500);
                    }else{
                        // GTM
                        var goodsInfoList = callBackResult.goodsInfoList;
                        
                        if(!!goodsInfoList && goodsInfoList.length > 0) {
                            var goodsNos = [];
                            var goodsNms = "";
                            var salePrc = 0;
                            for(var i = 0 ; i < goodsInfoList.length ; i++) {
                                if(i > 0) {
                                    goodsNms += "|";
                                }
                                goodsNos.push(goodsInfoList[i].goodsNo+goodsInfoList[i].itemNo);
                                goodsNms += goodsInfoList[i].goodsNm;
                                salePrc += (parseInt(goodsInfoList[i].salePrc) - parseInt(goodsInfoList[i].cpnRtAmtVal));
                            }
                            dataLayer.push({
                                'productId' : goodsNos,             //상품ID
                                'productName' : goodsNms,           //상품명
                                'productAmt' : salePrc.toString()   //상품가격
                            });
                        }
                        
                        if( optChgYn == 'Y' ){
                            location.reload();
                            return false;
                        }
                        
                        //  메인 목록에서 장바구니 담기 했을 시
                        if ( listYn == 'Y' ){
                            //  기존 화면 닫기
                            common.popLayerClose('basketOption');
                            
                            //  장바구니 등록 완료 화면으로 이동
                            var url = _baseUrl + "common/getCartCompleteAjax.do";
                            common.Ajax.sendRequest("POST",url,data,common._callCartComplete);               
                        } else if( listYn == 'A' || listYn == 'S' ) {
                            common.cart.regCartCnt += 1;
                        } else {
                            common.cart.showBasket();       // 기존 모바일은 분기없이 해당처리만 함
                        }
                    }
                    
                }else{
                    if( listYn == 'A' )
                        return false;
                    
                    if(!!data.message && data.message.length < 100){
                        alert(data.message);
                    }else{
                        alert("장바구니 등록에 실패하였습니다.");  
                    }
//                    if (data.message == "-9990") {
//                        alert("판매중지된 상품은 장바구니에 담을 수 없습니다.");
//                    } else if (data.message == "0") {
//                        alert("재고가 부족하여 상품을 장바구니에 담을 수 없습니다.");
//                    } else if (data.message == "99") {
//                        alert("장바구니는 99개까지만 담으실 수 있습니다");   
//                    } else {
//                        alert("장바구니 등록에 실패하였습니다.");
//                    }
                }
            };
            
            // 퀵배송변수 quickYn SET
            try{
                quickYn = cartSelGetInfoList[0].quickYn;
                if(quickYn != "Y" && quickYn != "N"){
                    quickYn = "N";
                }
            }catch(e){
                quickYn = "N";
            }

            var isValid = this.validation(cartSelGetInfoList, directYn, saveTp, optChgYn, cartYn);

            if (isValid) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(this.jsonParam),
                    contentType: "application/json;charset=UTF-8",
                    dataType : 'json',
                    async: false,
                    cache: false,
                    success: callback,
                    error : function(e) {
                        console.log(e);
                        alert("장바구니 등록에 실패하였습니다.");
                    }
                });
            }
            
            return callBackResult;
        },
        
        validation : function(cartSelGetInfoList, directYn, saveTp, optChgYn, cartYn) {
            // 파라메터의 validation 처리
            console.log("ins.validation :: cartSelGetInfoList="+cartSelGetInfoList);
            var isValid = true;
            if(cartSelGetInfoList == null) {
                var msg = "죄송합니다. 고객센터에 문의해 주세요.";

                this.jsonParam = false;
                isValid = false;
            }
            
            //행사안내레이어 장바구니버튼 진입시사용
            if(location.href.indexOf("getCart.do?cartNo=") > 0){
                directYn = "Y";
            }
            
            if(isValid) {
                
                var qDeliveVal = $(":input:radio[name=qDelive]:checked").val();
                
                if(qDeliveVal==undefined || qDeliveVal == null || qDeliveVal == ""){
                    qDeliveVal = "N";
                }
                
                this.jsonParam =   {
                        drtPurYn : directYn
                        ,saveTp : saveTp
                        ,optChgYn : optChgYn
                        ,cartYn : cartYn
                        ,quickYn : qDeliveVal
                        ,opCartBaseList : cartSelGetInfoList
                    };
            }
            return isValid;
        }, 
        
        showBasket : function(){
            
            common.cart.regCartCnt += 1;
            
            //찜 알림레이어 On
            $(".layerAlim").removeClass("Basket");
            $(".layerAlim").removeClass("zzimOn");
            $(".layerAlim").removeClass("zzimOff");
            
            $(".layerAlim").addClass("Basket");
            $(".layerAlim > p").html("<p>장바구니에 <br />저장되었습니다.</p>");
            $(".layerAlim").fadeIn(500);
            
            setTimeout(function(){
                $(".layerAlim").fadeOut(800);
            }, 1200);

            // mantis : 0000561 로 인한 class 제거 주석처리 필요
//            setTimeout(function(){
//                $(".layerAlim").removeClass("Basket");
//            }, 2000);
        },
        
        updHeaderCartCnt : function(){
            if (common.isLogin()) {                
                //장바구니 수량 업데이트
                $.ajax({
                    type: "POST",
                    url: _baseUrl + "common/getCartCntJson.do",
                    contentType: "application/json;charset=UTF-8",
                    dataType : 'json',
                    async: false,
                    cache: false,
                    success: function(data) {                        
                        if(typeof data != 'undefined' && data != null){
                            if(data == 0){                                
                                $("#mHeader").find(".basket .cnt").css('display','none');
                            }else{
                                $("#mHeader .basket").append('<span class="cnt">');
                                $("#mHeader").find(".basket .cnt").css('display','block');
                                $("#mHeader").find(".basket .cnt").text(data);
                            }
                        }
                    }
                });
            }
        }
};

/*--------------------------------------------------------------------------------*\
* 최근 본 상품 공통
\*--------------------------------------------------------------------------------*/
$.namespace("common.recentGoods");
common.recentGoods = {

        param : "",

        init : function(){
            common.recentGoods.getList('slide');

            //더보기 버튼 이벤트 처리
            $('.late-conts > .btnMore').on('click', function(){
                common.link.moveRecentList();
            });
        },

        getGoodsList : function(goodsCnt){
            var goodsNo = "";
            // default : 최대 5개

            //BI Renewal. 20190918. nobbyjin. - 쿠키설정오류 수정
            //cookie = new Cookie('local', 1, 'M');
            cookie = new Cookie(30);

            if ( cookie.get('productHistory') != undefined && cookie.get('productHistory') != "" ){

                var jsonStr = JSON.parse(cookie.get('productHistory'));
                var cnt = jsonStr.length;

                if(goodsCnt != undefined && goodsCnt != null && goodsCnt != ""){
                    cnt = goodsCnt
                }

                for(var i=0; i <jsonStr.length; i++){
                    if(i < cnt && jsonStr[i].goodsNo != null && jsonStr[i].goodsNo != ""){
                        if ( i == jsonStr.length-1 ){
                            goodsNo += jsonStr[i].goodsNo;
                        }else{
                            goodsNo += jsonStr[i].goodsNo + ",";
                        }
                    }

                    if(i == cnt){
                        break;
                    }
                }
            }
            return goodsNo;
        },

        getList : function(type, callback){

            // 메인 - drawer메뉴 하단 - 최근본상품 5개 제한
            var goodsNo;

            if (type == "slide") {
                goodsNo = common.recentGoods.getGoodsList(5);
            } else {
                goodsNo = common.recentGoods.getGoodsList();
            }

            if(goodsNo != null && goodsNo != ""){
                param = {
                        goodsInfo : goodsNo ,
                        type : type ,
                        pagingFlag : "N"
                };

                common.Ajax.sendRequest("get"
                        , _baseUrl + "mypage/getRecentListAjax.do"
                        , param
                        , function(res) { common.recentGoods.getRecentListAjaxCallback(res, type, callback); }
                );
            } else {
                if (type == "slide") {
                    //BI Renewal. 20190918. nobbyjin. - nodata 영역 수정
                    //$(".nodataTxt").show();
                    if ($(".nodataTxt").length>0) {
                        $(".nodataTxt").show();
                    } else {
                        $(".late-conts .mlist3v-goods").append("<li class='nodataTxt'>최근 본 상품이 없어요</li>");
                    }
                }else{
                    $(".sch_no_data2").show();
                }
            }

        },

        /**
         * 최근 본 상품 정보조회
         */
        getRecentListAjaxCallback : function(res, type, callback){
            if (res.trim() == "" ) {
                $(".sch_no_data2").show();
            } else {                
                if (type == "slide") {
                    $(".late-conts .mlist3v-goods").append(res);

                    /** 이미지 속성 교체 (LazyLoad 사용 안함) **/
                    $(".late-conts .mlist3v-goods > li img").each(function(){
                        $(this).attr("src", $(this).attr("data-original"));
                    });
                    
                    common.wish.init();

                    $(document).resize();
                } else {
                    $("#mContents div.totalBox").show();
                    $("#mContents").append(res);
                    $(".totalBox > .cnt strong").text($(".mlist5v-goods li").length);
                    common.setLazyLoad();
                }

                var jsonStr =  JSON.parse(cookie.get('productHistory'));
                var chkChg = false;
                
                // 쿠키 30개 넘어가는 상품번호 삭제처리
                for( var j=0; j<jsonStr.length; j++){
                    if (j >= 30) {
                        delete jsonStr.splice(j, 1);
                        chkChg = true;
                    }
                }
                //쿠키에 값 셋팅
                //cookie.set('productHistory', JSON.stringify(jsonStr));
                if (chkChg) cookie.set('productHistory', JSON.stringify(jsonStr));

                setTimeout(function() {
                    //링크 처리
                    common.bindGoodsListLink(".late-conts .mlist3v-goods");
                    
                    common.recentGoods.bindWebLog();
                }, 100);
            }

            if (callback != undefined) {
                callback();
            }
        },
        
        //웹로그 바인드
        bindWebLog : function() {
            //공지사항
            $(".late-conts .mlist3v-goods li").each(function(idx) {
                $(this).find(".goodsList").bind("click", function() {
                    common.wlog("home_drawer_recent_goods" + (idx + 1));
                });
            });

        },

		//BI Renewal. 20190918. nobbyjin. - 유틸바 최근본 상품 셋팅.
        getLast : function(){
            var cookie = new Cookie();
            var sHistory = cookie.get('productHistoryL') || '',
                 jsonStr = sHistory == '' ? '' : JSON.parse(cookie.get('productHistoryL'));
            if (jsonStr!='') {
                $("#footerTabRecentGoods .thum").show();
                $("#footerTabRecentGoods .thum").html("<img src='" + jsonStr.goodsImg + "' onerror='common.errorImg(this);' alt='최근 본 상품'/>");
            } else {
                $("#footerTabRecentGoods .thum").hide();
            }
        }
};

/*--------------------------------------------------------------------------------*\
* 찜 클릭 공통
\*--------------------------------------------------------------------------------*/
$.namespace("common.wish");
common.wish = {

        init : function(){
            //체크 처리.
            common.wish.checkWishList();
        },

        bindEvent : function() {

            $('.jeem').unbind("click");

            //찜 클릭 이벤트
            $('.jeem').bind('click', function(){
                //로그인 체크
//                if(common.loginChk()){
                    var param = {
                            goodsNo : $(this).attr("data-ref-goodsNo")
                    };

                    if($(this).hasClass("mClick")){
                        //off
                        common.wish.delWishLst(param, $(this));
                    }else{
                        //on
                        common.wish.regWishLst(param, $(this));
                    }
//                }
            });

        },

        loadData : function() {
            //로그인 여부에 따라 찜목록 저장.
            if(common.isLogin()){
                var wishListJson = sessionStorage.getItem("wishList");

                //없으면 조회
                if (wishListJson == null || wishListJson.trim() == "") {

                    common.wish.isLoading = false;

                    //저장된 화면이 없을 경우 html 조회
                    $.ajax({
                        type: "POST",
                        url: _baseUrl + "mypage/getWishListJson.do",
                        data: null,
                        dataType : 'text',
                        async: false,
                        cache: false,
                        success: function(data) {

                            try {
                                var jsonObject = $.parseJSON(data);
                                sessionStorage.setItem("wishList", data);

                            } catch (e) {}

                            common.wish.isLoading = true;
                        },
                        error: function() {
                            common.wish.isLoading = true;
                        }
                    });
                }

            } else {
                //제거
                sessionStorage.removeItem("wishList");
            }
        },

        checkWishList : function() {

            common.wish.bindEvent();

            setTimeout(function() {
                //정보 조회
                common.wish.loadData();

                var wishListJson = sessionStorage.getItem("wishList");

                $('.jeem').removeClass("mClick");
                
                if (wishListJson != null && wishListJson.trim() != "") {
                    try {
                        var jsonObject = $.parseJSON(wishListJson);

                        var goodsList = jsonObject.goodsList;

                        for (var i = 0; i < goodsList.length; i++) {
                            $(".jeem[data-ref-goodsNo='" + goodsList[i] + "']").addClass("mClick");
                        }
                    } catch (e) {}
                }

            }, 100);
        },

        regWishLst : function(param, obj){
            if (param.goodsNo == undefined || param.goodsNo == "" ) {
                alert("등록이 실패하였습니다.\n상품정보가 없습니다.");
                return;
            }
            
            var callBackResult = "";

            common.Ajax.sendRequest(
                      "POST"
                    , _baseUrl + "mypage/regWishLstAjax.do"
                    , param
                    , function(res) {
                        callBackResult = res;
                        common.wish.regWishLstAjaxCallback(res, obj)
                    }
                    , false
            );
            
            return callBackResult;
        },

        regWishLstAjaxCallback : function(res, obj){

            
            setTimeout(function() {
                //목록 갱신을 위해 제거
                sessionStorage.removeItem("wishList");
                common.wish.checkWishList();
            }, 100);

            var result = res.trim();
            if (result != '000') {
                if (result == '100') {
                    //로그인 실패
                    if (!common.loginChk()) {
                        return ;
                    }
                } else if (result == '200') {
                    if(common.isLogin()){
                        //  성인상품인데 로그인은 되어 있으나 성인인증이 안되었을 경우
                        common.link.moveRegCertPage("Y",location.href);
                    }else{
                        //성인인증필요
                        //로그인 성인체크
                        common.link.moveLoginPage("Y", location.href);
                    }
                    return;
                } else if (result == '500') {
                    //개수 초과
                    alert("쇼핑 찜은 99개 까지만 담으실 수 있습니다.");
                    return;
                } else if (result == '600') {
                    //개수 초과
                    //alert("이미 찜한 상품입니다.");
                    alert("쇼핑찜리스트에 저장된 상품입니다."); 
                    return;
                }
            }
            
            if (obj != undefined) {
                obj.addClass("mClick");
            }
            //찜 알림레이어 On
            $(".layerAlim").removeClass("Basket");
            $(".layerAlim").removeClass("zzimOff");
            $(".layerAlim").removeClass("zzimOn");
            
            $(".layerAlim").addClass("zzimOn");
            $(".layerAlim > p").html("찜!<br />되었습니다.");
            $(".layerAlim").fadeIn(500);

            setTimeout(function(){
                $(".layerAlim").fadeOut(800);
            }, 1200);

//            setTimeout(function(){
//                $(".layerAlim").removeClass("zzimOn");
//            }, 2300);
        },

        delWishLst : function(param, obj){
            if (param.goodsNo == undefined || param.goodsNo == "" ) {
                alert("삭제가 실패하였습니다.\n상품정보가 없습니다.");
                return;
            }
            
            var callBackResult = "";

            common.Ajax.sendRequest(
                    "POST"
                  , _baseUrl + "mypage/delWishLstAjax.do"
                  , param
                  , function(res) {
                      callBackResult = res;
                      common.wish.delWishLstAjaxCallback(res, obj)
                  }
                  , false
            );
            
            return callBackResult;
        },

        delWishLstAjaxCallback : function(res, obj){
            //목록 갱신을 위해 제거
            sessionStorage.removeItem("wishList");
            common.wish.checkWishList();

            var result = res.trim();
            if (result != '000') {
                if (result == '100') {
                    //로그인 실패
                    if (!common.loginChk()) {
                        return ;
                    }
                }
            }
            
            if (obj != undefined) {
                obj.removeClass("mClick");
            }
            //찜 알림레이어 Off
            $(".layerAlim").removeClass("Basket");
            $(".layerAlim").removeClass("zzimOff");
            $(".layerAlim").removeClass("zzimOn");
            
            $(".layerAlim").addClass("zzimOff");
            $(".layerAlim > p").html("찜이<br />취소 되었습니다.");
            $(".layerAlim").fadeIn(500);

            setTimeout(function(){
                $(".layerAlim").fadeOut(800);
            }, 1200);

//            setTimeout(function(){
//                $(".layerAlim").removeClass("zzimOff");
//            }, 2000);
        },

        delAllWishLst : function(param){
            common.Ajax.sendRequest(
                    "POST"
                  , _baseUrl + "mypage/delWishLstAjax.do"
                  , param
                  , common.wish.delWishLstAjaxCallback
            );
        }
};

$.namespace("common.coupon");
common.coupon = {
        /**
         ***** 쿠폰 등록 레이어 팝업 *****
         *
         * 팝업 호출 및 쿠폰 등록
         * 
         * getRegCouponForm : arg = true 이면 화면 갱신
         *                    arg     없으면 화면 갱신하지 않음                    
         **/
        reload : false
        ,
        getRegCouponForm : function(reload){
            if(typeof reload != "undefined"){
                if(reload){
                    common.coupon.reload = true;
                }
            }else{
                common.coupon.reload = false;
            }
            common.Ajax.sendRequest("POST"
                , _baseUrl + "common/popup/getRegCouponFormPop.do"
                , null
                , function(res) {
                    $("#pop-full-contents").html(res);
                    common.popFullOpen("쿠폰 등록");
                }
            );
        },
        regCouponAjax : function(){
            if(!common.isLogin() && confirm("로그인 후 신청하실 수 있습니다.\r\n로그인 페이지로 이동하시겠습니까?")){
                common.link.moveLoginPage("", location.href);
            }else{
                var rndmVal = $("#rndmVal").val();
    
                if(rndmVal.length <= 0){
                    alert("쿠폰번호를 입력해주세요.");
                    return false;
                }
                common.Ajax.sendRequest("POST"
                    , _baseUrl + "common/regCouponJson.do"
                    , { rndmVal : rndmVal}
                    , common.coupon.regCouponAjaxCallback
                );
            }
        },
        regCouponAjaxCallback : function(res){
            if(typeof res != "undefined"){
                if(res == '000'){
                    alert("쿠폰이 등록되었습니다. 등록된 쿠폰은 'MY>쿠폰'에서 확인 가능합니다.");

                    if(common.coupon.reload){
                        document.location.reload();
                    }else{
                        common.popFullClose();
                    }
                }else{
                    alert(res);
                }
            }
        }
};

$.namespace("common.zipcode.pop");
common.zipcode.pop = {
        fnCallback : '',
        
        defaultId : "search-zipcode-pop",

        init : function(popCallback, id){
            common.zipcode.pop.fnCallback = popCallback;
            
            if(arguments.length == 1 || !id){
                id = common.zipcode.pop.defaultId;
            }
            
            $('#'+id).click(function(){
                common.setScrollPos2();//클릭 시, 스크롤 위치값 localstorage 저장
                $('#pop-full-contents').html('');
                
                $('#pop-full-contents').load(_baseUrl + 'common/popup/searchZipcodePop.do', function(){
                    
                    common.popFullOpen('우편번호 찾기', mcommon.popup.zipcode.popupClear);
                    
                    mcommon.popup.zipcode.init(common.zipcode.pop.fnCallback);
                });
            });
        }
};

$.namespace("common.zipcodequick.pop");
common.zipcodequick.pop = {
        fnCallback : '',
        
        defaultId : "search-zipcode-pop",

        init : function(popCallback, id){
            common.zipcodequick.pop.fnCallback = popCallback;
            common.zipcodequick.pop.quickYn = 'Y'; //당인배송에서 호출하는 경우 추가
            
            if(arguments.length == 1 || !id){
                id = common.zipcodequick.pop.defaultId;
            }
            
            $('#'+id).click(function(){

                common.setScrollPos();
                $('#searchZipcode').find('#LAYERPOP01-contents').html('');
                $('#searchZipcode').find('#LAYERPOP01-title').html('우편번호 찾기');
                
                $('#searchZipcode').find('#LAYERPOP01-contents').load(_baseUrl + 'common/popup/searchZipcodePop.do', function(){
                    common.setScrollPos();
                    common.popLayerOpen2("searchZipcode");

                    mcommon.popup.zipcode.init(common.zipcodequick.pop.fnCallback);
                });
            });
        },
        
        /** 배송지 등록 폼 **/
        deliveryRegistForm : function(){
            var url = _baseUrl + "goods/getDeliveryRegistFormAjax.do";
            var data ={};
            common.Ajax.sendRequest("POST",url,data,common.zipcodequick.pop._callBackDeliveryRegistForm);
        },
        
        // 2019-11-18
        /** 배송지 등록 폼 (기존 배송지 선택 팝업로직과 다름, 배송지 추가하는 부분만 다룬다.) **/
        deliveryRegistFormOnlyRegist : function(){
            var url = _baseUrl + "goods/getDeliveryRegistFormOnlyRegistAjax.do";
            var data ={};
            common.Ajax.sendRequest("POST",url,data,common.zipcodequick.pop._callBackDeliveryRegistFormOnlyRegist);
        },
        
        /** 배송지 등록(장바구니) 폼 **/
        deliveryRegistCartForm : function(){
            common.zipcodequick.pop.cartYn = 'Y';
            var url = _baseUrl + "cart/getDeliveryRegistFormCartAjax.do";
            var data ={};
            common.Ajax.sendRequest("POST",url,data,common.zipcodequick.pop._callBackDeliveryRegistForm);
        },
        
        /** 배송지 등록 폼 콜백 **/
        _callBackDeliveryRegistForm : function(res){
            common.setScrollPos();
            var cDiv = $(res.trim());
            $("#pop-full-wrap").html(cDiv);
            $('#rmitCellSctNo').css({'width':'30.6%'});
            $('#rmitTelRgnNo').css({'width':'30.6%'});
            
            common.popFullOpen("배송지 추가");   
            common.zipcodequick.pop.init(mmypage.deliveryForm.selectedZipcodeCallback);
        },
        
        /** 배송지 등록 폼 콜백 **/
        _callBackDeliveryRegistFormOnlyRegist : function(res){
            common.setScrollPos();
            var cDiv = $(res.trim());
            $("#pop-full-wrap").html(cDiv);
            $('#rmitCellSctNo').css({'width':'30.6%'});
            $('#rmitTelRgnNo').css({'width':'30.6%'});
            
            common.popFullOpen("배송지 추가");   
            common.zipcodequick.pop.init(mmypage.deliveryForm.selectedZipcodeCallback);
        },
        
        /** 배송지 등록  닫기 **/
        deliveryRegistFormClose : function(){
            common.popFullClose();
            common.zipcodequick.pop.quickYn = ''; 
            
            mgoods.detail.todayDelivery.todayDeliveryList();
        },
        
        /** 배송지 등록  닫기 (기존 배송지 팝업 로직X, 팝업 닫힘.) **/
        deliveryRegistFormCloseOnlyRegist : function(){
            common.popFullClose();
            common.zipcodequick.pop.quickYn = ''; 
            
            /*mgoods.detail.todayDelivery.todayDeliveryList();*/
        },
        
        /** 배송지 등록  후 닫기 **/
        deliveryRegistFormAfterClose : function(){
            common.popFullClose();
            common.zipcodequick.pop.quickYn = ''; 
            
            mgoods.detail.todayDelivery.registTodayDeliverySelect();
        },
        
        // 2019-11-18
        /** 배송지 등록  후 닫기  (기존 배송지 팝업 로직X, 등록 후 팝업 닫힘.) **/
        deliveryRegistFormAfterCloseOnlyRegist : function(data){
            console.log("deliveryRegistFormAfterCloseOnlyRegist() 호출됨.");
            
            common.popFullClose();
            common.zipcodequick.pop.quickYn = ''; 
            
            /*mgoods.detail.todayDelivery.todayDeliveryListOnPage();*/
            mgoods.detail.todayDelivery.registTodayDeliverySelect2();
            
        },
        
        /** 배송지 등록  닫기 **/
        deliveryRegistFormCartClose : function(){
            common.popFullClose();
            common.zipcodequick.pop.quickYn = ''; 
            common.zipcodequick.pop.cartYn = '';
            window.location.reload();
            //mgoods.detail.todayDelivery.todayDeliveryList();
        }
        
         
};

$.namespace("common.bann");
common.bann = {
        
        getPopInfo : function(popType) {
//            var cookie = new Cookie('local', 1, 'D');
            var cookie = new Cookie(1);

            if ( cookie.get('popBannHistory') != undefined && cookie.get('popBannHistory') != "" ) {
                var popBannHistoryStr = cookie.get('popBannHistory');
                var jsonStr =  JSON.parse(popBannHistoryStr);
                
                for(var i=0; i <jsonStr.length; i++) {
                    if (jsonStr[i].popType == popType) {
                        return jsonStr[i];
                    }
                }
            }
            return null;
        },
        
        setPopInfo : function(popType, compareKey) {
//            var cookie = new Cookie('local', 1, 'D');
            var cookie = new Cookie(1); 
            var bannArr = new Array();

            if ( cookie.get('popBannHistory') != undefined && cookie.get('popBannHistory') != "" ) {
                var popBannHistoryStr = cookie.get('popBannHistory');
                
                //신규
                if (popBannHistoryStr == "") {
                    var bannInfo = new Object();
                    
                    bannInfo.popType = popType;
                    bannInfo.compareKey = compareKey;
                    bannInfo.regDtime = new Date();
                       
                    bannArr.push(bannInfo);
                    
                } else {
                    var jsonStr =  JSON.parse(popBannHistoryStr);
                    
                    var hasInfo = false;
                    
                    for(var i=0; i <jsonStr.length; i++) {
                        if (jsonStr[i].popType == popType) {
                            jsonStr[i].compareKey = compareKey;
                            jsonStr[i].regDtime = new Date();
                            hasInfo = true;
                        }
                        
                        bannArr.push(jsonStr[i]);
                    }
                    
                    if (!hasInfo) {
                        var bannInfo = new Object();
                        
                        bannInfo.popType = popType;
                        bannInfo.compareKey = compareKey;
                        bannInfo.regDtime = new Date();
                           
                        bannArr.push(bannInfo);
                        
                    }
                }

            } else {
                var bannInfo = new Object();
                
                bannInfo.popType = popType;
                bannInfo.compareKey = compareKey;
                bannInfo.regDtime = new Date();
                   
                bannArr.push(bannInfo);
            }
            cookie.set('popBannHistory', JSON.stringify(bannArr)); 
        },
}

/*
 * 행사사은품선택팝업띄우기
 */
$.namespace('common.popLayer.promGift');
common.popLayer.promGift = {
        jsonParam : false,
        
        openPromGiftPop : function(vGoodsNo,vItemNo,vPromNo) {
            var url = _baseUrl + "cart/promGiftPopAjax.do";
            
            var callback = function(res) {
                $("#layerPop").html(res);
                common.popLayerOpen("LAYERPOP01");
                
                //mcart.base.setLazyLoad('seq');
                var popPos = $('#LAYERPOP01').height()/2;
                $('#LAYERPOP01').css('margin-top',-(popPos));
                
                //GET레이어의 상품 수가  1인경우 자동실행
                if($('li[name=selPopInfo]').length == 1){
                    var stockQty = parseInt( $('li[name=selPopInfo]').eq(0).attr('stockQty') );
                    
                    if(stockQty > 0){
                        var setItemCnt = 0;
                        if(stockQty > getItemCnt) 
                            setItemCnt = getItemCnt;
                        else
                            setItemCnt = stockQty;
                        
                        $('li[name=selPopInfo]').eq(0).find('input[name=promGiftAmount]').val(setItemCnt);
                        $('li[name=selPopInfo]').eq(0).attr("class","on");
                        
                        // 기 선택한 추가상품 수량
                        $("p.choiceTxt span i").text(setItemCnt);
                    }
                } else {
                    //buy상품군의 수량정보를 가져와 품절체크추가
                    //레이어창의 상품군
                    var totalCnt = 0;
                    $('li[name=selPopInfo]').each(function(){
                        var layerPopObj = $(this);
                        
//                        $("div.prd_item_box").each(function(){
                        $("p.item_number").each(function(){
                            var buyObj = $(this);
                            var buyGoodsNo = buyObj.find("input[name=sGoodsNo]").val();
                            var buyItemNo = buyObj.find("input[name=itemNo]").val();
                            
                            buyGoodsNo = (buyGoodsNo == undefined) ? $("#goodsNo").val() : buyGoodsNo;
                            buyItemNo = (buyItemNo == undefined) ? $("#itemNo").val() : buyItemNo;
                            
                            //buy상품군의 상품정보와 레이어창의 상품정보가 같으면
                            if(layerPopObj.attr('goodsNo') == buyGoodsNo && layerPopObj.attr('itemNo') == buyItemNo){
                                var buyGoodsCnt = parseInt(buyObj.find('input[type=tel].num').val());
                                var getGoodsCnt = parseInt(layerPopObj.attr('stockQty'));
                                
                                // 상품상세에서 오늘드림 여부가 선택된 경우 오늘드림 재고를 보고 판단해야함 jwkim
                                if($("#deliveDay").prop("checked")==true){
                                    getGoodsCnt = $("#quickAvalInvQty").val();
                                }
                                
                                //buy상품군의 수량이 실재고 수량보다 크거나 같으면
                                if(buyGoodsCnt >= getGoodsCnt){
                                    //조건만족시 수량0으로 셋팅하고 품절처리
                                    layerPopObj.find('input[name=promGiftAmount]').val(0);
                                    layerPopObj.attr('class','soldout');
                                    layerPopObj.find('div.img').append("<span>일시품절</span>");
                                    layerPopObj.find('input[name=promGiftAmount]').attr('disabled','disabled');
                                    layerPopObj.find('input[name=promGiftAmount]').siblings().attr('disabled','disabled');
                                    layerPopObj.removeClass("on");
                                } else {
                                    layerPopObj.attr('stockQty', getGoodsCnt - buyGoodsCnt);
                                }
                            }
                        });
                        
                        totalCnt += parseInt( layerPopObj.find("input[name=promGiftAmount]").val() );
                    });
                    
                    // 현재 선택된 수량 갱신
                    $("p.choiceTxt span i").text(totalCnt);      // 기 선택한 추가상품 수량
                    
                    // 일시품절 상품 하단 정렬 시작
                    var getItemList = [];
                    var getItemSoldOutList = [];

                    $("ul.listPlusPrd li[name=selPopInfo]").each(function(){
                        if($(this).hasClass("soldout")){
                            getItemSoldOutList.push($(this));
                        } else {
                            getItemList.push($(this));
                        }
                    });

                    $("ul.listPlusPrd").html();

                    $(getItemList).each(function(){
                        $("ul.listPlusPrd").append($(this));
                    });

                    $(getItemSoldOutList).each(function(){
                        $("ul.listPlusPrd").append($(this));
                    });
                    // 일시품절 상품 하단 정렬 끝
                }
            };
            
            var isValid = this.validation(vGoodsNo,vItemNo,vPromNo);
            
            if (isValid) {
                _ajax.sendRequest("GET", url, this.jsonParam, callback);
            }
        },
        /**
         *   파라메터의 validation 처리
         */
        validation : function(vGoodsNo,vItemNo,vPromNo) {
            var isValid = true;
            if(vGoodsNo == null || vGoodsNo == '' || vItemNo == null || vItemNo == '' || vPromNo == null || vPromNo == '') {
                var msg = "죄송합니다. 고객센터(1522-0882)로 문의해 주세요.";

                this.jsonParam = false;
                isValid = false;
                return isValid;
            }
            
            var buyItemCnt = 0;
            $("div.prd_cnt_box[promNo=" + vPromNo + "]").each(function(){
                buyItemCnt += parseInt( $(this).find("input.num").val() );
            });
            
            var buyCondStrtQtyAmt = parseInt( $("div.event_info.item_" + vGoodsNo + vItemNo).attr("buyCondStrtQtyAmt") );
            var getItemCnt = parseInt( buyItemCnt / buyCondStrtQtyAmt );
            
            var quickYn = $(":input:radio[name=qDelive]:checked").val();
            if(typeof(quickYn) == "undefined"){
                quickYn = $("#quickYn").val();
            }
            
            if(isValid) {
                this.jsonParam =   {
                        "goodsNo" : vGoodsNo,
                        "itemNo" : vItemNo,
                        "promNo" : vPromNo,
                        "buyItemCnt" : buyItemCnt,
                        "getItemCnt" : getItemCnt,
                        "quickYn" : quickYn
                    };
            }
            return isValid;
        },
        /**
         * 추가상품의 수량 조절
         */
        calcQty : function(optionKey, operator) {
            if(mcart.popLayer.promGift.focusOutFlag){
                mcart.popLayer.promGift.focusOutFlag = false;
                return false;
            }
            
            var oldVal = parseInt( $("input[name=promGiftAmount]#cartCnt_" + optionKey).val() );
            $("input[name=promGiftAmount]#cartCnt_" + optionKey).data('old', oldVal);
            
            if(operator == "plus"){
                if(oldVal >= getItemCnt){
                    alert('선택하실 수 있는 추가 상품은 최대 '+getItemCnt+'개 입니다.');
                    return false;
                }
                
                $("input[name=promGiftAmount]#cartCnt_" + optionKey).val(++oldVal).trigger("focusout");
            } else if(operator == "minus"){
                if(oldVal <= 0)
                    return false;
                
                $("input[name=promGiftAmount]#cartCnt_" + optionKey).val(--oldVal).trigger("focusout");
            } else {
                return false;
            }
        }
};

/*--------------------------------------------------------------------------------*\
* String Object Prototype
\*--------------------------------------------------------------------------------*/
String.prototype.isEmpty = function() {
    return (this == null || this == '' || this == 'undefined' || this == 'null');
};
// alert("isEmpty="+"".isEmpty());


/**
 *
 * 숫자여부 체크 함.
 * 사용 예)
 *
 * "100".isNumber()
 * "-100,000".isNumber(1)
 *
 * @param opt
 *            1 : (Default)모든 10진수
 *            2 : 부호 없음
 *            3 : 부호/자릿수구분(",") 없음
 *            4 : 부호/자릿수구분(",")/소숫점
 *
 * @return true(정상) or false(오류-숫자아님)
 *
 */
String.prototype.isNumber = function(opt) {
    // 좌우 trim(공백제거)을 해준다.
    value = String(this).replace(/^\s+|\s+$/g, "");

    if (typeof opt == "undefined" || opt == "1") {
        // 모든 10진수 (부호 선택, 자릿수구분기호 선택, 소수점 선택)
        var regex = /^[+\-]?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    } else if (opt == "2") {
        // 부호 미사용, 자릿수구분기호 선택, 소수점 선택
        var regex = /^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    } else if (opt == "3") {
        // 부호 미사용, 자릿수구분기호 미사용, 소수점 선택
        var regex = /^[0-9]+(\.[0-9]+)?$/g;
    } else {
        // only 숫자만(부호 미사용, 자릿수구분기호 미사용, 소수점 미사용)
        var regex = /^[0-9]$/g;
    }

    if (regex.test(value)) {
        value = value.replace(/,/g, "");
        return isNaN(value) ? false : true;
    } else {
        return false;
    }
};
//alert("isNumber="+("1-00".isNumber()));


String.prototype.nvl = function(s) {
    return this.isEmpty() ? (s ? s : '') : this+'';
};
String.prototype.startWith = function(str) {
    if (this === str)    return true;

    if (str.length > 0)
        return str === this.substr(0, str.length);
    else
        return false;
};
String.prototype.endWith = function(str) {
    if (this == str)    return true;

    if (String(str).length > 0)
        return str === this.substr(this.length - str.length, str.length);
    else
        return false;
};
String.prototype.bytes = function()
{    // 바이트 계산.
    var b = 0;
    for (var i=0; i<this.length; i++) b += (this.charCodeAt(i) > 128) ? 2 : 1;
    return b;
};
String.prototype.nl2br = function() {
    return this.replace(/\n/g, "<br />");
};
String.prototype.toMoney = function() {
    var s = (this.nvl('0')).trim();
    if (isFinite(s)) {
        while((/(-?[0-9]+)([0-9]{3})/).test(s)) {
            s = s.replace((/(-?[0-9]+)([0-9]{3})/), "$1,$2");
        }
        return s;
    }
    else {
        return this;
    }
};
String.prototype.toNegative = function() {
    return this == '0' ? this : "- " + this;
};

/**
 * val 문자열을 len 길이만큼 왼쪽에 char 문자를 붙여서 반환 한다.
 *
 * 사용 예) "A".lpad(5, "0") => "0000A"
 *
 * @param val
 *            문자열
 * @param len
 *            생성할 문자열 길이
 * @param char
 *            해당 길이만큼 왼쪽에 추가할 문자
 */
String.prototype.lpad = function(len, char) {
    var val = String(this);
    if (typeof(char)!="string" && typeof(len)!="number") {
        return val;
    }
    char = String(char);
    while(val.length + char.length<=len) {
        val = char + val;
    }

    return val;
};
// alert("A".lpad(5, "0"));

/**
 * val 문자열을 len 길이만큼 오른쪽에 char 문자를 붙여서 반환 한다.
 *
 * 사용 예) "A".rpad(5, "0") => "A0000"
 *
 * @param val
 *            문자열
 * @param len
 *            생성할 문자열 길이
 * @param char
 *            해당 길이만큼 오른쪽에 추가할 문자
 */
String.prototype.rpad = function(len, char) {
    var val = String(this);
    if (typeof(char)!="string" && typeof(len)!="number") {
        return val;
    }
    char = String(char);
    while(val.length + char.length<=len) {
        val = val + char;
    }

    return val;
};
//alert("A".rpad(5, "0"))

String.prototype.numberFormat = function() {
// return this;
// return $.number(this);
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};


String.prototype.getBytesLength = function() {
    var s = this;
    for(b = i = 0;c = s.charCodeAt(i++);b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
    return b;
};

String.prototype.getTransSpace = function() {
    return this.split(" ").join("&nbsp;");
};


/*--------------------------------------------------------------------------------*\
* Number Object Prototype
\*--------------------------------------------------------------------------------*/
Number.prototype.toMoney = function() {
    return String(this).toMoney();
};


Number.prototype.numberFormat = function() {
// return this
// return $.number(this);
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};


/**
 * val 문자열을 len 길이만큼 왼쪽에 char 문자를 붙여서 반환 한다.
 *
 * 사용 예) (123).lpad(5,"0") => "00123"
 *
 * @param val
 *            문자열
 * @param len
 *            생성할 문자열 길이
 * @param char
 *            해당 길이만큼 왼쪽에 추가할 문자
 */
Number.prototype.lpad = function(len, char) {
    return String(this).lpad(len,char);
};
//alert((123).lpad(5,"0"));

/**
 * val 문자열을 len 길이만큼 오른쪽에 char 문자를 붙여서 반환 한다.
 *
 * 사용 예) (123).rpad(5,"0") => "12300"
 *
 * @param val
 *            문자열
 * @param len
 *            생성할 문자열 길이
 * @param char
 *            해당 길이만큼 오른쪽에 추가할 문자
 */
Number.prototype.rpad = function(len, char) {
    return String(this).rpad(len,char);
}
//alert((123).rpad(5,"0"));

/*--------------------------------------------------------------------------------*\
* Date Object Prototype
\*--------------------------------------------------------------------------------*/
/**
 *
 * 포멧에 맞는 날짜 문자열을 꺼낸다.
 *
 * 사용 예)
 * > new Date().format("yyyy-MM-dd hh:mm:ss E a/p")
 * > => 2016-11-24 11:00:31 목요일 오전
 *
 *
 *
 */
Date.prototype.format = function(format) {
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;

    if (!d.valueOf()) return " ";
    if(format == undefined) return " ";
    format = String(format);

    return format.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy":   return (d.getFullYear() % 1000).lpad(2, "0");
            case "MM":   return (d.getMonth() + 1).lpad(2, "0");
            case "dd":   return d.getDate().lpad(2, "0");
            case "E":    return weekName[d.getDay()];
            case "HH":   return d.getHours().lpad(2, "0");
            case "hh":   return ((h = d.getHours() % 12) ? h : 12).lpad(2, "0");
            case "mm":   return d.getMinutes().lpad(2, "0");
            case "ss":   return d.getSeconds().lpad(2, "0");
            case "a/p":  return d.getHours() < 12 ? "오전" : "오후";
            default:     return $1;
        }
    });
};
//alert(new Date().format("yyyy-MM-dd hh:mm:ss E a/p"));


/**
 * 일수를 계산한 날짜를 반환 한다.
 *
 * 사용 예)
 * new Date().addDate(1);  : 내일날짜 반환
 * new Date().addDate(0);  : 오늘날짜 반환
 * new Date().addDate(-1); : 어제날짜 반환
 *
 * @param dateCount
 *            더할 일수
 *
 * @return 계산되어 생성된 Date Object
 *
 */
Date.prototype.addDate = function(dateCount) {
    return new Date(this.valueOf() + (dateCount * (24*60*60*1000)) );
};
//alert(new Date().addDate(1).addDate(1));




/*--------------------------------------------------------------------------------------*\
* Cookie object
* 2019.08.13_신종민_(SR : 3033094)_cName 분기처리 추가
*  요구사항 : 모바일 홈 팝업배너 '하루동안 보지않기' 당일 23시 59분까지 보이지 않게 수정    
\*--------------------------------------------------------------------------------------*/
var Cookie = function(expiresDay) {
    var expdate = (typeof expiresDay == 'number') ? expiresDay : 1;
    return {
        get : function(cName) {
            cName = cName + '=';
            var cookieData = document.cookie;
            var start = cookieData.indexOf(cName);
            var cValue = '';
            if(start != -1){
                 start += cName.length;
                 var end = cookieData.indexOf(';', start);
                 if(end == -1)end = cookieData.length;
                 cValue = cookieData.substring(start, end);
            }
            return unescape(cValue);
        },
        set : function(cName, cValue, expireDays) {
          
          if(cName == 'popBannHistory'){
            
            // 현재일의 23시 59분을 파라미터로 던지기
            var d = new Date();
            var dY = d.getFullYear();
            var dM = (d.getMonth()).lpad(2, "0");
            var dD = d.getDate().lpad(2, "0");
            var hExpr = (((23 * 60) + 59) * 60 * 1000);

            var dt = new Date(dY, dM, dD);
            var param = dt.getTime()+hExpr;
            
            this.setOwner(cName, cValue, param)
            
          }else{
            this.setOwner(cName, cValue, ((typeof expireDays == 'number' ? expireDays : expdate) * 24 * 60 * 60 * 1000))
          }
            return this;
        },
        setOwner : function(cName, cValue, expire) {
            var expdate = new Date();
            
            if(cName == 'popBannHistory'){              
              expdate.setTime(expire);
            }else{
              expdate.setTime(expdate.getTime() + (typeof expire == 'number' ? expire : (expdate * 24 * 60 * 60 * 1000)));
            }
            
            document.cookie = cName+"=" + cValue + "; path=/; domain="+document.domain+"; expires=" + expdate.toGMTString();
        },
        remove : function(name) {
            return this.set(name, '', -1);
        },
        getItem : function(name) {
            return this.get(name);
        },
        setItem : function(name, value) {
            this.set(name, value);
        },
        removeItem : function(name) {
            this.remove(name);
        },
        clear : function() {
            return;
        }
    };
};


/*--------------------------------------------------------------------------------*\
* Cache object
\*--------------------------------------------------------------------------------*/
var Cache = function(type, span/* integer */, format/* s, m, h, d, M, y, w */) {
    var _cacheType  = (typeof type != 'string' || type == '') ? 'cache' : type; // cache
                                                                                // ||
                                                                                // local
                                                                                // ||
                                                                                // session
    var _span       = (typeof span == 'number') ? span : 0;
    var _format     = (typeof format == 'string') ? format : '';
    var _storage    = null;
    var _expires    = getCacheExpires(_span, _format);
    var _default    = {
            set : function() { return;},
            get : function() { return '';},
            isStatus : function() { return false;},
            remove : function() { return; },
            clear : function() { return; }
        };


    if (_cacheType == 'session') {
        if (!window.sessionStorage) return _default;
        _storage= window.sessionStorage;
        _expires= (_span != 0) ? _expires : getCacheExpires(12, 'h'); // 12
                                                                        // hours
    }
    else if (_cacheType == 'cache') {
        if (!window.localStorage) return _default;
        _storage= window.sessionStorage;
        _expires= (_span != 0) ? _expires : getCacheExpires(5, 'm'); // 5
                                                                        // minutes
    }
    else if (_cacheType == 'local') {
        if (!window.localStorage) return _default;
        _storage = window.localStorage;
        _expires= (_span != 0) ? _expires : getCacheExpires(7, 'd'); // 7
                                                                        // days
    }
    else if (_cacheType == 'cookie') {
        _storage = com.lotte.smp.Cookie(1);
        _expires= (_span != 0) ? _expires : getCacheExpires(1, 'd'); // 1
                                                                        // days
    }
    else {
        return _default;
    }

    function getCacheExpires(s, f) {
        var exp = 0;
        switch(f) {
            case 's' : exp = 1;         break;
            case 'm' : exp = 60;        break;
            case 'h' : exp = 3600;      break;  // 60 * 60
            case 'd' : exp = 86400;     break;  // 60 * 60 * 24
            case 'w' : exp = 604800;    break;  // 60 * 60 * 24 * 7
            case 'M' : exp = 2592000;   break;  // 60 * 60 * 24 * 30
            case 'y' : exp = 31536000;  break;  // 60 * 60 * 24 * 365
        }
        return s * exp;
    }

    return {
        type    : _cacheType,
        storage : _storage,
        expires : _expires,
        set : function(name, value, expires) {
            if (typeof name != 'string' || name == '') return;
            if (value == 'undefined') return;
            if (expires=='undefined' || typeof expires != 'number') { expires = this.expires; }

            var date = new Date();
            var schedule= Math.round((date.setSeconds(date.getSeconds()+expires))/1000);

            this.storage.setItem(this.type +'@'+ name, value);
            this.storage.setItem(this.type +'@time_' + name, schedule);

            return this;
        },
        get : function(name) {
            if (this.isStatus(name)) {
                return this.storage.getItem(this.type +'@'+ name);
            }
            else {
                return '';
            }
        },
        isStatus : function(name) {
            if (this.storage.getItem(this.type +'@'+ name) == null || this.storage.getItem(this.type +'@'+ name) == '')
                return false;

            var date = new Date();
            var current = Math.round(+date/1000);

            // Get Schedule
            var stored_time = this.storage.getItem(this.type +'@time_' + name);
            if (stored_time=='undefined' || stored_time=='null') { stored_time = 0; }

            // Expired
            if (stored_time < current) {
                this.remove(name);
                return false;
            } else {
                return true;
            }
        },
        remove : function(name) {
            this.storage.removeItem(this.type +'@'+ name);
            this.storage.removeItem(this.type +'@time_' + name);
        },
        clear : function() {
            for (var item in this.storage) {
                if (String(item).startWith(this.type)) {
                    this.storage.removeItem(item);
                }
            }
            // this.storage.clear();
        }
    };
};

jQuery.fn.serializeObject = function() {
    var obj = null;
    try{
        if ( this[0].tagName && this[0].tagName.toUpperCase() == "FORM" ) {
            var arr = this.serializeArray();
            if ( arr ) {
              obj = {};
              jQuery.each(arr, function() {
                obj[this.name] = this.value;
              });
            }
        }
    }
    catch(e) {}
    finally  {}

    return obj;
};

var lazyloadSeq = {
        data_attribute  : "original",
        selector : "",
        replaceClassNm : "",
        
        load : function(selector, classNm) {
            lazyloadSeq.selector = selector;
            lazyloadSeq.replaceClassNm = classNm;
            
            var imgList = $(lazyloadSeq.selector);
            var imgCnt = imgList.length;
            
            lazyloadSeq.loadRecursive(0, imgCnt, imgList);
        },
        
        loadRecursive : function(curImgIdx,imgCnt, list) {
            if (curImgIdx == undefined || curImgIdx == null) {
                curImgIdx = 0;
            }
            
            var targetImgCnt = imgCnt;
            
            if (targetImgCnt > curImgIdx) {
                var dstImg = list.eq(curImgIdx);
                var imgSrc = dstImg.attr("data-" + lazyloadSeq.data_attribute);
                
                if (dstImg.is("img")) {
                    dstImg.attr("src", imgSrc);
                } else {
                    dstImg.css("background-image", "url('" + imgSrc + "')");
                }
                
                dstImg.removeClass(lazyloadSeq.replaceClassNm).addClass("completed-" + lazyloadSeq.replaceClassNm);
//                console.log("img load : " + imgSrc);
//                setTimeout(function() {
                    lazyloadSeq.loadRecursive(++curImgIdx, targetImgCnt, list);
//                }, 10);
            }
        }
        
};

var scrollTrigger = {
        scrollStarted : false,
        scrollLooper : null,   
        
        init : function() {
            document.addEventListener('touchmove', function(){           
                
                if (scrollTrigger.scrollLooper != null) {
                    clearInterval(scrollTrigger.scrollLooper);     
                }
                
                scrollTrigger.scrollStarted = true;
                scrollTrigger.scrollLooper = setInterval(function() {                                                                                 
                    $(window).trigger("scroll");   
//                    console.log("trigger scroll");
                }, 200) 
            });     
            document.addEventListener('touchend', function(){                                                                         
                scrollTrigger.scrollStarted = false;
                $(window).trigger("scroll");                                                                                          
                setTimeout(function() {                                                                                               
                    if(scrollTrigger.scrollStarted == false) {                                                                                      
                        clearInterval(scrollTrigger.scrollLooper);     
                        console.log("end trigger scroll");
                        
                        scrollTrigger.scrollLooper = null;
                    }                                                                                                                 
                }, 1000);                                                                                                             
            });
        }
};

